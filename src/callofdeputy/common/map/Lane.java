package callofdeputy.common.map;

import callofdeputy.client.Team;
import callofdeputy.common.Building;

import java.util.ArrayList;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;


public class Lane {
    private int id;
    List<Cell> cells;
    List<Path> paths;
    List<Cell> startLine, endLine;
    Direction startLineDirection, endLineDirection;
    Building start, end;

    Lane(int id) {
        this.id = id;
        cells = new ArrayList<>();
        paths = new ArrayList<>();
        startLine = new ArrayList<>();
        endLine = new ArrayList<>();
    }

    public int getID() {
        return id;
    }

    void addCell(Cell cell) {
        cells.add(cell);
    }

    public Cell[] getCells() {
        return cells.toArray(new Cell[cells.size()]);
    }

    boolean contains(Cell cell) {
        return Collections.binarySearch(cells, cell) >= 0 ||
                (start != null && start.contains(cell)) ||
                (end != null && end.contains(cell));
    }

    void setStartBuilding(Building MB) {
        this.start = MB;
    }

    Building getStartBuilding() {
        return start;
    }

    boolean hasStartBuilding() {
        return start != null;
    }

    void setEndBuilding(Building MB) {
        this.end = MB;
    }

    Building getEndBuilding() {
        return end;
    }

    boolean hasEndBuilding() {
        return end != null;
    }

    Building getBuildingOf(Team team) {
        return (start.getOwner() == team) ? start : end;
    }

    void setStartLine(List<Cell> startLine) {
        this.startLine.clear();
        this.startLine.addAll(startLine);
        if (startLine.get(0).x == startLine.get(1).x)
            startLineDirection = Direction.TOP;
        else
            startLineDirection = Direction.LEFT;
        if (startLine.get(0).getCellAtDir(startLineDirection.rotateCW()).type != CellType.LANE)
            startLineDirection = startLineDirection.inverse();
        start.setOrientation(startLineDirection);
    }

    public Cell[] getStartLine() {
        return startLine.toArray(new Cell[startLine.size()]);
    }

    void setEndLine(List<Cell> endLine) {
        this.endLine.clear();
        this.endLine.addAll(endLine);
        if (endLine.get(0).x == endLine.get(1).x)
            endLineDirection = Direction.TOP;
        else
            endLineDirection = Direction.LEFT;
        if (endLine.get(0).getCellAtDir(endLineDirection.rotateCW()).type != CellType.LANE)
            endLineDirection = endLineDirection.inverse();
        end.setOrientation(endLineDirection);
    }

    public Cell[] getEndLine() {
        return endLine.toArray(new Cell[endLine.size()]);
    }

    void complete() {
        Cell cell;
        LinkedList<Cell> openCells = new LinkedList<>(cells);
        cells.clear();
        while (!openCells.isEmpty()) {
            cell = openCells.getFirst();
            int index = indexOf(cell);
            if (cell.type == CellType.LANE && index < 0) {
                cells.add(-1 - index, cell);
                Collections.addAll(openCells, cell.getNeighbours());
            }
            openCells.removeFirst();
        }
    }

    int indexOf(Cell cell) {
        return Collections.binarySearch(cells, cell);
    }

    void refreshPaths() {
        // startCell is last cell in the startLine in direction of startLineDirection
        paths.clear();
        ArrayList<Cell> closedCells = new ArrayList<>();
        Cell startCell = startLine.get(0);
        while (startLine.contains(startCell.getCellAtDir(startLineDirection)))
            startCell = startCell.getCellAtDir(startLineDirection);
        int pathID = 0, step = 1;
        if (startLineDirection == Direction.BOTTOM || startLineDirection == Direction.RIGHT) {
            pathID = 4;
            step = -1;
        }
        for (; pathID < 5 && pathID >= 0; pathID += step) {
            Path currentPath = new Path(start, end, this, pathID);
            Direction pathDir = startLineDirection.rotateCW();
            Cell currentCell = startCell;
            Cell nextCell;
            while (true) {
                currentPath.addCell(currentCell);
                closedCells.add(currentCell);
                if (endLine.contains(currentCell))
                    break;
                pathDir = pathDir.inverse();
                do {
                    pathDir = pathDir.rotateCW();
                    nextCell = currentCell.getCellAtDir(pathDir);
                } while (nextCell == null || nextCell.type != CellType.LANE || closedCells.contains(nextCell));
                currentCell = nextCell;
            }
            paths.add(currentPath);
            startCell = startCell.getCellAtDir(startLineDirection.inverse());
        }
    }

    public Path[] getPaths() {
        return paths.toArray(new Path[paths.size()]);
    }

    public Path getPath(int pathID) {
        for (Path path : paths)
            if (path.getID() == pathID)
                return path;
        return null;
    }

    public Path getMiddlePath() {
        return getPath(2);
    }

    public String toString() {
        Cell[] cells = getCells();
        String result = "[Lane:" + id + ", Cells: ";
        for (Cell cell : cells)
            result += cell + ", ";
        return result + "]";
    }
}
