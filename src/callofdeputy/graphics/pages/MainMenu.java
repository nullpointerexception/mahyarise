package callofdeputy.graphics.pages;

import callofdeputy.graphics.util.ClipPlayer;
import callofdeputy.graphics.util.ImageButton;
import callofdeputy.graphics.util.ImagePanel;
import callofdeputy.graphics.util.Page;

import javax.swing.*;
import java.awt.*;
import java.util.ArrayList;

public class MainMenu extends Page {
    public static ClipPlayer music;
    // cover
    private ImagePanel cover = new ImagePanel("cover.png", true),
            cover_text = new ImagePanel("cover_text.png", true),
            cover_laser = new ImagePanel("cover_laser.png", true);
    private ImagePanel[] wallpapers;
    private int lastCover = -1, prevLastCover = -1;
    private long lastChangeTime = -1;
    private long coverChangeDuration = 15000;
    private int transitionAlpha = 0, transitionAlphaStep = 4;
    private boolean showLaser;
    private int textAlpha = 0, textAlphaStep = 4;
    public double laserShowChance = 0.05;
    // grid of buttons
    private JPanel grid;
    private GridBagConstraints lastConstraints;

    public MainMenu() {
        // Load wallpapers
        ArrayList<ImagePanel> walls = new ArrayList<>();
        boolean cont = true;
        for (int i = 0; cont && i < 9; i++)
            try {
                walls.add(new ImagePanel("wall-" + (i+1) + ".jpg", true));
            } catch (Exception ignore) {
                cont = false;
            }
        wallpapers = walls.toArray(new ImagePanel[walls.size()]);

        // Style of the page
        setOpaque(false);
        setLayout(new BorderLayout());
        grid = new JPanel(new GridBagLayout());
        grid.setOpaque(false);
        lastConstraints = new GridBagConstraints();
        lastConstraints.gridx = lastConstraints.gridy = 0;
        lastConstraints.gridwidth = lastConstraints.gridheight = 1;
        lastConstraints.weighty = 1;
        lastConstraints.fill = GridBagConstraints.VERTICAL;
//        // scroll pane
//        JScrollPane scroll = new JScrollPane(grid,
//                ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS,
//                ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
//        adjustScrollBar(scroll.getHorizontalScrollBar());
//        adjustScrollBar(scroll.getVerticalScrollBar());
//        scroll.setBorder(null);
////        scroll.getViewport().setBackground(new Color(0, 0, 0));
////        scroll.setOpaque(false);
        add(grid, BorderLayout.SOUTH);
        addButtons();
    }

    private void adjustScrollBar(JScrollBar scrollBar) {
        scrollBar.setPreferredSize(new Dimension(0, 0));
        scrollBar.setUnitIncrement(64);
    }

    private void addButtons() {
        String files[] = new String[] {
                "btn_new_game",
        };
        ImageButton[] buttons = new ImageButton[files.length];
        for (int i = 0; i < files.length; i++) {
            buttons[i] = new ImageButton(files[i] + ".png",
                    files[i] + "_over.png", null);
            buttons[i].setPreferredSize(new Dimension(200, 44));
            buttons[i].setOpaque(false);
            addButton(buttons[i]);
        }
        // setting actions
        buttons[0].addActionListener(e -> screen.showPage("game play"));
    }

    public Component addButton(Component comp) {
        grid.add(comp, lastConstraints);
        lastConstraints.gridx++;
        return comp;
    }

    @Override
    public int getRepaintTime() {
        return 50;
    }

    @Override
    public void onHide() {
        music.fadeOut(2000);
    }

    @Override
    public void paint(Graphics g) {
        Graphics2D g2 = (Graphics2D) g;
        paintCover(g2);
        super.paint(g);
    }

    public void paintCover(Graphics2D g) {
        if (lastChangeTime + coverChangeDuration < System.currentTimeMillis()) {
            lastChangeTime = System.currentTimeMillis();
            prevLastCover = lastCover;
            lastCover++;
            transitionAlpha = 1;
        }
        if (lastCover > wallpapers.length)
            lastCover = 0;
        paintCover(g, lastCover);
        if (transitionAlpha > 0) {
            Composite old = g.getComposite();
            g.setComposite(AlphaComposite.SrcOver.derive(1 - (float) transitionAlpha/256));
            paintCover(g, prevLastCover);
            g.setComposite(old);
            transitionAlpha += transitionAlphaStep;
            if (transitionAlpha > 255)
                transitionAlpha = 0;
        }
    }

    public void paintCover(Graphics2D g, int coverNum) {
        if (coverNum < 0)
            return;
        if (coverNum == 0) {
            cover.setBounds(g.getClipBounds());
            cover.paint(g);
            paintCoverText(g);
            paintCoverLaser(g);
        } else {
            wallpapers[coverNum-1].setBounds(g.getClipBounds());
            wallpapers[coverNum-1].paint(g);
        }
    }

    public void paintCoverLaser(Graphics2D g) {
        if (Math.random() < laserShowChance)
            showLaser = !showLaser;
        if (showLaser) {
            cover_laser.setBounds(g.getClipBounds());
            cover_laser.paint(g);
        }
    }

    public void paintCoverText(Graphics2D g) {
        Composite old = g.getComposite();
        cover_text.setBounds(g.getClipBounds());
        if (textAlpha > 0) {
            g.setComposite(AlphaComposite.SrcOver.derive((float) textAlpha/256));
            cover_text.paint(g);
        }
        textAlpha += textAlphaStep;
        if (textAlpha > 255) {
            textAlpha = 255;
            textAlphaStep = -textAlphaStep;
        } else if (textAlpha < 0) {
            textAlpha = 0;
            textAlphaStep = -textAlphaStep;
        }
        g.setComposite(old);
    }
}
