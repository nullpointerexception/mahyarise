package Test;

import javax.swing.*;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;

/**
 * Created by Hadi on 5/25/14.
 */
public abstract class OnlineInfo extends Thread {
    private boolean alive = true;
    private JTextArea textArea;

    public OnlineInfo(String title, int width, int height) {
        setDaemon(true);
        textArea = new JTextArea();
        JFrame frame = new JFrame("Online Info -- " + title);
        frame.setSize(width, height);
        JScrollPane scroll = new JScrollPane(textArea,
                JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED, JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);
        frame.add(scroll);
        frame.addWindowListener(new WindowListener() {
            @Override
            public void windowOpened(WindowEvent e) {
            }

            @Override
            public void windowClosing(WindowEvent e) {
                alive = false;
            }

            @Override
            public void windowClosed(WindowEvent e) {
            }

            @Override
            public void windowIconified(WindowEvent e) {
            }

            @Override
            public void windowDeiconified(WindowEvent e) {
            }

            @Override
            public void windowActivated(WindowEvent e) {
            }

            @Override
            public void windowDeactivated(WindowEvent e) {
            }
        });
        frame.setVisible(true);
    }

    public OnlineInfo(String title) {
        this(title, 700, 100);
    }

    public OnlineInfo() {
        this("Online Info");
    }

    public abstract String getInfo();

    @Override
    public void run() {
        while (alive) {
            textArea.setText(getInfo());
            try {
                sleep(50);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
}
