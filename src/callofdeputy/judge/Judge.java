package callofdeputy.judge;

import callofdeputy.common.exceptions.MahyariseExceptionBase;
import callofdeputy.common.map.*;
import callofdeputy.common.*;
import callofdeputy.client.*;
import callofdeputy.graphics.GameLogic;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by Hadi on 4/28/14.
 */
public class Judge extends JudgeAbstract {
    public Team ce = GameLogic.getTeam(TeamID.CE);
    public Team math = GameLogic.getTeam(TeamID.MATH);
    public Team[] teams = new Team[] {ce, math};
    public Map map = GameLogic.getMap();
    public Game game = GameLogic.getGame();
    public HashMap<GameObjectID , GameObject> idToObject = new HashMap<>();

    @Override
    public void setMapSize(int i, int i2) {
        map.setMapSize(i, i2);
    }

    private int xChange(int x) {
        return x;
    }

    private int yChange(int y) {
        return getMapHeight() - y - 1;
    }

    @Override
    public int getMapWidth() {
        return map.getWidth();
    }

    @Override
    public int getMapHeight() {
        return map.getHeight();
    }

    @Override
    public void loadMap(int[][] map) {
        for (int i = 0; i < map.length; i++)
            for (int j = 0; j < map[0].length; j++)
                setMapCellType(j, i, map[i][j]);
    }

    @Override
    public void setMapCellType(int col, int row, int type) {
        map.setCellType(col, row, cellTypeFromInt(type));
    }

    @Override
    public int getMapCellType(int i, int i2) {
        return cellTypeToInt(map.getCellAt(i, i2).getType());
    }

    @Override
    public void setup() {
        map.refresh();
        GameObject[] buildings = map.getGameObjects();
        for (GameObject building : buildings)
            idToObject.put(building.getID(), building);
    }

    @Override
    public GameObjectID createAttacker(int i, int i2, int i3, int i4) throws MahyariseExceptionBase {
        GameObject attacker = game.createAttacker(getTeamID(i), getAttackerType(getTeamID(i), i2), i4, i3);
        idToObject.put(attacker.getID(), attacker);
        return attacker.getID();
    }

    @Override
    public GameObjectID createTower(int i, int i2, int i3,  int i4) throws MahyariseExceptionBase {
        GameObject tower = game.createTower(getTeamID(i), getTowerType(i2), i3, i4);
        idToObject.put(tower.getID(), tower);
        return tower.getID();
    }

    @Override
    public void purchaseTeamPowerup(int i, int i2) throws MahyariseExceptionBase {
        //System.exit(0);
        game.purchaseTeamPowerup(getTeamID(i), getPowerupType(i2));
    }

    @Override
    public void purchaseTowerPowerup(int i, GameObjectID gameObjectID, int i2) throws MahyariseExceptionBase {
        //System.exit(0);
        GameObject object = idToObject.get(gameObjectID);
        if (object.getClass() == Attacker.class)
            purchaseTeamPowerup(i, i2);
        else
            game.purchaseTowerPowerup(getTeamID(i), (Tower)idToObject.get(gameObjectID),getPowerupType(i2));
    }

    @Override
    public int getMoney(int i) {
        //System.exit(0);
        return game.getTeam(getTeamID(i)).getMoney();
    }

    @Override
    public int[] getTeamPowerups(int i) {
        //System.exit(0);
        ArrayList<PowerupType> arraylist =  game.getTeam(getTeamID(i)).getTeamPowerups();
        int[] arrayOfInt = new int[arraylist.size()];
        for(int j = 0 ; j < arrayOfInt.length ; ++j)
            arrayOfInt[j] = getIdOfPowerupType(arraylist.get(j));
        return arrayOfInt;
    }

    @Override
    public HashMap<String, Integer> getInfo(GameObjectID gameObjectID) throws MahyariseExceptionBase {
        //System.exit(0);
        GameObject object = idToObject.get(gameObjectID);
        HashMap<String, Integer> info = new HashMap<>();
        info.put(HEALTH, object.getHealth());
        if (object.getPosition() != null) {
            Cell position = object.getPosition();
            info.put(ROW, position.getY());
            info.put(COLOUMN, position.getX());
        } else {
            info.put(ROW, null);
            info.put(COLOUMN, null);
        }
        info.put(TEAM_ID, object.getOwner().getID() == TeamID.CE ? TEAM_CE : TEAM_MATH);
        info.put(IS_ALIVE, object.isAlive() ? 1 : 0);
        info.put(VALUE, object.getValue());
        if (object instanceof Attacker) {
            Attacker attacker = (Attacker) object;
            info.put(ATTACK, attacker.getPower());
            info.put(RELOAD_TIME, attacker.getReloadTime());
            info.put(RANGE, attacker.getRange());
        } else if (object instanceof Tower) {
            Tower tower = (Tower) object;
            info.put(RELOAD_TIME, tower.getReloadTime());
            info.put(RANGE, tower.getRange());
            info.put(TANK_ATTACK, tower.getTankPower());
            info.put(INFANTRY_ATTACK, tower.getInfaPower());
        } else if (object.getClass() == Building.class) {
            Building building = (Building) object;
            info.put(ORIENTATION, building.getOrientation() == Direction.TOP ? 1 : 0);
        }
        return info;
    }

    @Override
    public GameObjectID[] getBuildingID(int i, int i2) {
        //System.exit(0);
        GameObjectID[] result = null;
        if (i2 == BUILDING_TYPE_HQ)
            result = new GameObjectID[] {game.getTeam(getTeamID(i)).getHQ().getID()};
        if (i2 == BUILDING_TYPE_MILITARY_BASE) {
            Building[] MBs = game.getTeam(getTeamID(i)).getMBs();
            result = new GameObjectID[] {MBs[0].getID(), MBs[1].getID(), MBs[2].getID()};
        }
        return result;
    }

    @Override
    public GameObjectID[] getInRange(GameObjectID gameObjectID) throws MahyariseExceptionBase {
        //System.exit(0);
        GameObject object = idToObject.get(gameObjectID);
        int range = 0;
        if (object instanceof Attacker)
            range = ((Attacker)object).getRange();
        if (object instanceof Tower)
            range = ((Tower)object).getRange();
        GameObject[] inRange = object.getPosition().getObjectsInRange(range);
        GameObjectID[] ids = new GameObjectID[inRange.length];
        for (int i = 0; i < inRange.length; i++)
            ids[i] = inRange[i].getID();
        return ids;
    }

    @Override
    public GameObjectID getTarget(GameObjectID gameObjectID) throws MahyariseExceptionBase {
        //System.exit(0);
        return idToObject.get(gameObjectID).getTarget().getID();
    }

    int i = 0;
    @Override
    public void next50milis() {
        game.nextTurn();
    }

    @Override
    public void startTimer() {
        //System.exit(0);
        game.startTimer();
    }

    @Override
    public void pauseTimer() {
        //System.exit(0);
        game.pauseTimer();
    }

    @Override
    public float getTime() {
        //System.exit(0);
        return game.getElapsedTime();
    }

    @Override
    public void setMoney(int i, int i2) {
        //System.exit(0);
        game.getTeam(getTeamID(i)).setMoney(i2);
    }

    @Override
    public void updateInfo(GameObjectID gameObjectID, String s, Integer integer) throws MahyariseExceptionBase {
        //System.exit(0);
        GameObject object = idToObject.get(gameObjectID);
        Attacker attacker;
        Tower tower;
        switch (s) {
            case HEALTH: object.setHealth(integer); break;
            case ATTACK:
                if (object.getClass() != Attacker.class)
                    break;
                attacker = (Attacker) object;
                attacker.setPower(integer);
                break;
            case RELOAD_TIME:
                if (object.getClass() == Attacker.class) {
                    attacker = (Attacker) object;
                    attacker.setReloadTime(integer);
                } else if (object.getClass() == Tower.class) {
                    tower = (Tower) object;
                    tower.setReloadTime(integer);
                }
                break;
            case VALUE: object.setValue(integer); break;
            case RANGE:
                if (object.getClass() == Attacker.class) {
                    attacker = (Attacker) object;
                    attacker.setRange(integer);
                } else if (object.getClass() == Tower.class) {
                    tower = (Tower) object;
                    tower.setRange(integer);
                }
                break;
            case TANK_ATTACK:
                if (object.getClass() != Tower.class)
                    break;
                tower = (Tower) object;
                tower.setTankPower(integer);
                break;
            case INFANTRY_ATTACK:
                if (object.getClass() != Tower.class)
                    break;
                tower = (Tower) object;
                tower.setInfaPower(integer);
                break;
        }
    }

    @Override
    public void updateInfo(GameObjectID gameObjectID, HashMap<String, Integer> stringIntegerHashMap) throws MahyariseExceptionBase {
        //System.exit(0);
        stringIntegerHashMap.forEach((s, integer) -> {
            try {
                updateInfo(gameObjectID, s, integer);
            } catch (MahyariseExceptionBase mahyariseExceptionBase) {
                mahyariseExceptionBase.printStackTrace();
            }
        });
    }

    // Converters

    private CellType cellTypeFromInt(int cellType) {
        CellType type = null;
        switch (cellType) {
            case CELL_TYPE_LANE: type = CellType.LANE; break;
            case CELL_TYPE_HQ: type = CellType.HQ; break;
            case CELL_TYPE_MILITARY_BASE: type = CellType.MB; break;
            case CELL_TYPE_UNUSED: type = CellType.UNUSED;
        }
        return type;
    }

    private int cellTypeToInt(CellType type) {
        int cellType = -1;
        switch (type) {
            case LANE: cellType = CELL_TYPE_LANE; break;
            case HQ: cellType = CELL_TYPE_HQ; break;
            case MB: cellType = CELL_TYPE_MILITARY_BASE; break;
            case UNUSED: cellType = CELL_TYPE_UNUSED; break;
        }
        return cellType;
    }

    private TeamID getTeamID(int teamID) {
        if (teamID == TEAM_CE)
            return TeamID.CE;
        else
            return TeamID.MATH;
    }

    private AttackerType getAttackerType(TeamID owner, int attackerType) {
        if (owner == TeamID.CE)
            if (attackerType == ATTACKER_INFANTRY)
                return AttackerType.CE_INFANTRY;
            else
                return AttackerType.CE_TANK;
        else
        if (attackerType == ATTACKER_INFANTRY)
            return AttackerType.MATH_INFANTRY;
        else
            return AttackerType.MATH_TANK;
    }

    private TowerType getTowerType(int towerType) {
        switch (towerType) {
            case TOWER_TYPE_BLACK: return TowerType.BLACK;
            case TOWER_TYPE_ELECTRICITY: return TowerType.ELECTRICITY;
            case TOWER_TYPE_GAME: return TowerType.GAME;
            case TOWER_TYPE_GENERAL_MATH: return TowerType.GENERAL_MATH;
            case TOWER_TYPE_POISON: return TowerType.POISON;
            case TOWER_TYPE_TANK: return TowerType.TANK;
        }
        return null;
    }

    private int getIntTowerType(TowerType towerType) {
        switch (towerType) {
            case BLACK: return TOWER_TYPE_BLACK;
            case ELECTRICITY: return TOWER_TYPE_ELECTRICITY;
            case GAME: return TOWER_TYPE_GAME;
            case GENERAL_MATH: return TOWER_TYPE_GENERAL_MATH;
            case POISON: return TOWER_TYPE_POISON;
            case TANK: return TOWER_TYPE_TANK;
        }
        return -1;
    }

    private PowerupType getPowerupType(int powerupType) {
        switch (powerupType) {
            case PU_ATTACKER_HEALTH: return PowerupType.ATTACKER_HEALTH;
            case PU_ATTACKER_POWER: return PowerupType.ATTACKER_POWER;
            case PU_CE_ARMOR: return PowerupType.CE_ARMOR;
            case PU_CE_HEALTH: return PowerupType.CE_HEALTH;
            case PU_CE_PACE: return PowerupType.CE_PACE;
            case PU_MATH_DEC_VAL: return PowerupType.MATH_DEC_VAL;
            case PU_MATH_ECO: return PowerupType.MATH_ECO;
            case PU_MATH_PROFIT: return PowerupType.MATH_PROFIT;
            case PU_TOWER_AUTO_HEALING: return PowerupType.TOWER_AUTO_HEALING;
            case PU_TOWER_PACE: return PowerupType.TOWER_PACE;
            case PU_TOWER_POWER: return PowerupType.TOWER_POWER;
            case PU_TOWER_RANGE: return PowerupType.TOWER_RANGE;
        }
        return null;

    }

    private int getIdOfPowerupType(PowerupType type) {
        switch (type) {
            case ATTACKER_HEALTH: return PU_ATTACKER_HEALTH;
            case ATTACKER_POWER: return PU_ATTACKER_POWER;
            case CE_ARMOR: return PU_CE_ARMOR;
            case CE_HEALTH: return PU_CE_HEALTH;
            case CE_PACE: return PU_CE_PACE;
            case MATH_DEC_VAL: return PU_MATH_DEC_VAL;
            case MATH_ECO: return PU_MATH_ECO;
            case MATH_PROFIT: return PU_MATH_PROFIT;
            case TOWER_AUTO_HEALING: return PU_TOWER_AUTO_HEALING;
            case TOWER_PACE: return PU_TOWER_PACE;
            case TOWER_POWER: return PU_TOWER_POWER;
            case TOWER_RANGE: return PU_TOWER_RANGE;
        }
        return -1;
    }

}
