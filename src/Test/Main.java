package Test;

import callofdeputy.graphics.GameLogic;
import callofdeputy.graphics.GameScreen;
import callofdeputy.graphics.gameplay.MapGraphics;

/**
 * Created by Hadi on 6/1/2015 11:17 AM.
 */
public class Main {

    public static void main(String... args) {
        int k = 80;
        int map4[][] = new int[k][k];
        for (int i = 0; i < k; i++)
            for (int j = 0; j < k; j++)
                if (i < 40 && j < 40)
                    map4[i][j] = TestMaps.map3[i][j];
                else
                    map4[i][j] = 13;
        GameLogic.init();
        GameLogic.setMap(map4);
        MapGraphics.load();
//        new GameScreen() {
//            long lastPaintTime = 0;
//
//            @Override
//            public void paint(Graphics g) {
//                long time = System.currentTimeMillis();
//                if (time - lastPaintTime > 5000) {
//                    lastPaintTime = time;
//                    BufferedImage img = new BufferedImage(getWidth(), getHeight(), BufferedImage.TYPE_INT_ARGB);
//                    g = img.getGraphics();
//                    super.paint(g);
//                    try {
//                        ImageIO.write(img, "png", new File("screenshot-" + lastPaintTime + ".png"));
//                    } catch (IOException e) {
//                        e.printStackTrace();
//                    }
//                } else super.paint(g);
//            }
//        }.start();
        new GameScreen().start();
//        BufferedImage img = ImageUtils.getOrLoad("MB_HOR.png");
//        BufferedImage cop = GraphicsEnvironment.getLocalGraphicsEnvironment().getDefaultScreenDevice().getDefaultConfiguration().createCompatibleImage(img.getWidth(), img.getHeight(), Transparency.TRANSLUCENT);
//        cop.createGraphics().drawImage(img, 0, 0, null);
//        JFrame frame = new JFrame();
//        JPanel panel;
//        frame.setIgnoreRepaint(true);
//        frame.setSize(1300, 700);
//        frame.setVisible(true);
//        frame.createBufferStrategy(2);
//        BufferStrategy bs = frame.getBufferStrategy();
//        frame.add(panel = new JPanel() {
//            int i = 0;
//            {
//                setIgnoreRepaint(true);
//            }
//
//            @Override
//            public void paint(Graphics g) {
//                int num = 50;
////                long st = System.currentTimeMillis();
//                long s = System.currentTimeMillis();
//                for (int j = 0; j < 10000; j++)
//                    g.drawImage(cop, j/1000, j%1000, null);
//                long e = System.currentTimeMillis();
//                System.out.println("1: " + (e-s));
////                i++;
//                s = System.currentTimeMillis();
//                for (int j = 0; j < 10000; j++)
//                    g.drawImage(img, j/1000, j%1000, null);
//                e = System.currentTimeMillis();
//                System.out.println("2: " + (e-s));
////                long en = System.currentTimeMillis();
////                if (en - st > 2)
////                   System.out.println("paint i=" + i + " in " + (en - st));
//            }
//        });
//        Graphics g = bs.getDrawGraphics();
//        panel.paint(g);
//        bs.show();
//        g.dispose();
//        new Thread() {
//            @Override
//            public void run() {
//                try {
//                    sleep(1000);
//                    long dif = 0;
//                    for (int i = 0; i < 1000; i++) {
////                        sleep(20);
//                        long st = System.currentTimeMillis();
//                        Graphics g = bs.getDrawGraphics();
//                        panel.paint(g);
//                        bs.show();
//                        g.dispose();
//                        long en = System.currentTimeMillis();
//                        dif += en - st;
//                        yield();
//                    }
//                    System.out.println(dif);
//                } catch (InterruptedException e) {
//                    e.printStackTrace();
//                }
//            }
//        }.start();

//        new Timer(5000, e -> {
//            ArrayList<Integer> list = new ArrayList<>();
//            int maxi = 0;
//            for (int i = 0; i < MapGraphics.times.length; i++) {
//                list.add(MapGraphics.times[i]);
//                if (MapGraphics.times[i] > MapGraphics.times[maxi])
//                    maxi = i;
//            }
//            System.out.println(list);
//            System.out.println(maxi + " " + MapGraphics.times[maxi] + " times");
//        }).start();

//        new ObjectOutputStream(new FileOutputStream("save.save"))
//        System.out.println("starting judge.");
//        long start = System.nanoTime();
//        Judge judge = new Judge();
//        int[][] map = map4;
//        judge.setMapSize(map.length, map[0].length);
//        judge.loadMap(map);
//        judge.setup();

//        GraphicsInterface gui = new GraphicsInterface(judge);
//        judge.setGUI(gui);
//        gui.start();

//        judge.setMoney(judge.TEAM_CE, 1000000);
//        judge.setMoney(judge.TEAM_MATH, 1000000);
//        for (int i = 0; i < 200; i++) {
//            judge.createAttacker(judge.TEAM_CE, 9, (i%15)/5, i%5);
//            judge.createAttacker(judge.TEAM_MATH, 9, (i%15)/5, i%5);
//        }
//
//        for (int i = 0; i < 10000; i++) {
//            if (i % 1000 == 0)
//                System.out.println("Turn " + i);
//            judge.next50milis();
//        }
//        System.out.println("10000 turns completed!");
//        long end = System.nanoTime();
//        System.out.println("total time:" + (end - start)/1000000000.0);
//        judge.idToObject.forEach((id, obj) -> System.out.println(id + ": " + obj.getHealth()));
    }

}
