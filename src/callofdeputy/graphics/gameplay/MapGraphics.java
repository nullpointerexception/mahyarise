package callofdeputy.graphics.gameplay;

import callofdeputy.common.*;
import callofdeputy.common.map.Cell;
import callofdeputy.common.map.Map;
import callofdeputy.common.map.Path;
import callofdeputy.graphics.GameLogic;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

public class MapGraphics extends JPanel implements MouseMotionListener,
        MouseWheelListener, MouseListener {
    public static final Color bg = new Color(40, 37, 11).brighter();
    public static int size;
    public static Cell center;
    private static int cellSizes[] = new int[] {15, 18, 22, 26, 31, 37, 45, 54, 65};

    public static void load() {
        for (int size : cellSizes) {
            CellGraphics.renderScaled(size);
            GameObjectGraphics.renderScaled(size);
        }
    }

    private Map map;
    private int width;
    private int height;
    private int xOffset;
    private int yOffset;
    private int cellSizeIndex = 1;
    private int cellSize = cellSizes[cellSizeIndex];
    private GameObject sample;
    private Point samplePos;
    private Cell mark;
    private Color markColor;
    private ArrayList<Object> sortedCells; // including buildings

    public MapGraphics() {
        addMouseListener(this);
        addMouseMotionListener(this);
        addMouseWheelListener(this);
    }

    public void setMap(Map map) {
        this.map = map;
    }

    public void drawSample(GameObject o, Point pos) {
        sample = o;
        samplePos = pos;
    }

    public void deleteSample() {
        drawSample(null, null);
    }

    public void mark(Cell cell, Color color) {
        this.mark = cell;
        this.markColor = color;
    }

    public void unmark() {
        mark(null, null);
    }

//    public static int[] times = new int[200];

    @Override
    public void paint(Graphics g) {
        long s = System.currentTimeMillis();
        if (width == 0 && height == 0) // first paint
            renderScaled();
        g.setColor(bg);
        g.fillRect(0, 0, getWidth(), getHeight());
        g.translate(xOffset, yOffset);
        paintMap(g);
        paintSample(g);
        g.translate(-xOffset, -yOffset);
        long e = System.currentTimeMillis();
//        times[((int) (e - s))]++;
    }

    private void paintMap(Graphics g) {
        Point p = getLocationOnScreen();
        int minRow = getCellContains(p).getY() - 3;
        p.translate(0, getHeight());
        int minCol = getCellContains(p).getX() - 3;
        p.translate(getWidth(), 0);
        int maxRow = getCellContains(p).getY() + 3;
        p.translate(0, -getHeight());
        int maxCol = getCellContains(p).getX() + 3;
        g.translate(0, map.getWidth() * cellSize / 2);
        for (Object o : sortedCells) {
            // paint cell
            if (o instanceof Cell) {
                Cell c = (Cell) o;
                int i = c.getY(), j = c.getX();
                if (j < minCol || j > maxCol || i < minRow || i > maxRow)
                    continue;
                Point cc = getCellCenter(j, i);
                g.translate(cc.x, cc.y);
                CellGraphics.paintCell(map.getCellAt(j, i), cellSize, g);
                if (mark != null)
                    if (j == mark.getX() && i == mark.getY())
                        CellGraphics.paintMark(mark, markColor, cellSize, g);
                CellGraphics.paintGameObjects(map.getCellAt(j, i), cellSize, g);
                g.translate(-cc.x, -cc.y);
            }
            // paint building
            if (o instanceof Building) {
                Building b = (Building) o;
                Cell fc = b.firstCell(), lc = b.lastCell();
                Point cc = getCellCenter((fc.getX() + lc.getX())/2.0,
                        (fc.getY() + lc.getY())/2.0);
                g.translate(cc.x, cc.y);
                GameObjectGraphics.paint(b, cellSize, g);
                g.translate(-cc.x, -cc.y);
            }
        }
        g.translate(0, -map.getWidth() * cellSize / 2);
    }

    private void paintSample(Graphics g) {
        if (sample == null)
            return;
        Cell dest = getDestination(samplePos);
        Graphics2D g2 = (Graphics2D) g;
        Composite composite = g2.getComposite();
        g2.setComposite(AlphaComposite.SrcOver.derive(0.7f));
        if (dest == null) {
            Point pos = getLocationOnScreen();
            g.translate(samplePos.x - pos.x - xOffset, samplePos.y - pos.y - yOffset);
                GameObjectGraphics.paint(sample, cellSize, g);
            g.translate(-samplePos.x + pos.x + xOffset, -samplePos.y + pos.x + yOffset);
        } else {
            Point cc = getCellCenter(dest.getX(), dest.getY());
            g.translate(cc.x, cc.y + map.getWidth() * cellSize / 2);
                GameObjectGraphics.paint(sample, cellSize, g);
            g.translate(-cc.x, -cc.y - map.getWidth() * cellSize / 2);
        }
        g2.setComposite(composite);
    }

    public void addGameObject(Point onScreen) {
        Cell dest = getDestination(onScreen);
        if (dest == null)
            return;
        if (sample instanceof Attacker)
            map.createAttacker(sample.getOwner(),
                    ((Attacker) sample).getType(),
                    map.getPathContains(dest).getID(),
                    map.getLaneContains(dest).getID());
        if (sample instanceof Tower)
            map.createTower(sample.getOwner(),
                    ((Tower) sample).getType(),
                    dest.getX(), dest.getY());
    }

    private Cell getDestination(Point onScreen) {
        Cell cell = getCellContains(onScreen);
        if (cell == null)
            return null;
        if (sample instanceof Attacker) {
            Path path = map.getPathContains(cell);
            if (path == null)
                return null;
            sample.setOwner(GameLogic.getPlayerTeam());
            if (!map.canCreateAttacker(sample.getOwner(), ((Attacker) sample).getType(), path.getID(), path.getLane().getID()))
                return null;
            return path.getFirstCellOf(sample.getOwner().getID());
        } else if (sample instanceof Tower) {
            Path path = map.getPathContains(cell);
            if (path == null || path.getID() != 2)
                return null;
            sample.setOwner(GameLogic.getPlayerTeam());
            if (!map.canCreateTower(sample.getOwner(), ((Tower) sample).getType(), cell.getX(), cell.getY()))
                return null;
            return cell;
        }
        return null;
    }

    public Point getCellCenter(double col, double row) {
        return IsometricUtils.d2_to_iso((int) (col * cellSize) + cellSize / 2,
                (int) (row * cellSize) + cellSize / 2);
    }

    public Cell getCellContains(Point onScreen) {
        Point mapPos = getLocationOnScreen();
        Point d2 = IsometricUtils.iso_to_2d(onScreen.x - mapPos.x - xOffset,
                onScreen.y - mapPos.y - yOffset - map.getWidth()*cellSize/2);
        return map.getCellAt(d2.x / cellSize, d2.y / cellSize);
    }

    public void renderScaled() {
        boolean firstTime = width == 0 && height == 0;
        width = IsometricUtils.d2_to_iso(map.getWidth() * cellSize, map.getHeight() * cellSize).x;
        height = (map.getWidth() + map.getHeight()) * cellSize / 2;
        if (firstTime) {
            long st = System.currentTimeMillis();
            xOffset = (getWidth() - width) / 2;
            yOffset = (getHeight() - height) / 2;
            // sort cells and buildings by the layer they will be painted on
            ArrayList<Cell> cells = new ArrayList<>();
            int extraSize = 40;
            for (int i = -extraSize; i < map.getHeight() + extraSize; i++)
                for (int j = -extraSize; j < map.getWidth() + extraSize; j++)
                    cells.add(map.getCellAt(j, i));
            Comparator<Cell> xCompare = Comparator.comparing(Cell::getX).reversed();
            Comparator<Cell> cellComparator = xCompare.thenComparing(Cell::getY);
            cells.sort(cellComparator);
            sortedCells = new ArrayList<>();
            for (int i = 0; i < cells.size();) {
                Cell cell = cells.get(i);
                Building b = map.getBuildingContains(cell);
                if (b != null) {
                    if (b.firstCell() == cell) {
                        ArrayList<Cell> bCells = new ArrayList<>();
                        Collections.addAll(bCells, b.getCells());
                        bCells.sort(cellComparator);
                        sortedCells.addAll(bCells);
                        sortedCells.add(b);
                        cells.removeAll(bCells);
                        i = 0;
                    } else {
                        while (xCompare.compare(cell, cells.get(i)) == 0)
                            i++;
                    }
                    continue;
                }
                sortedCells.add(cell);
                cells.remove(i);
            }
            long en = System.currentTimeMillis();
            System.out.println("sort in " + (en - st));
        }
        CellGraphics.renderScaled(cellSize);
        GameObjectGraphics.renderScaled(cellSize);
    }

    public void zoom(int x, int y, int c) {
        int zoom = 0;
        if (c < 0 && cellSizeIndex < cellSizes.length - 1) {
            // zoom in
            zoom = cellSizes[cellSizeIndex+1] - cellSizes[cellSizeIndex];
            cellSizeIndex++;
        }
        if (c > 0 && cellSizeIndex > 0) {
            // zoom out
            zoom = cellSizes[cellSizeIndex-1] - cellSizes[cellSizeIndex];
            cellSizeIndex--;
        }
        if (zoom != 0) {
            xOffset -= zoom * (x - xOffset) / cellSize;
            yOffset -= zoom * (y - yOffset) / cellSize;
            cellSize = cellSize + zoom;
            renderScaled();
            move(new Point(0, 0), new Point(0, 0));
        }
    }

    public void move(Point from, Point to) {
        final int border = 20;
        xOffset += to.x - from.x;
        yOffset += to.y - from.y;
        xOffset = Math.min(xOffset, border);
        xOffset = Math.max(xOffset, getWidth() - width - border);
        xOffset += Math.min(xOffset, border);
        xOffset /= 2;
        yOffset = Math.min(yOffset, border);
        yOffset = Math.max(yOffset, getHeight() - height - border);
        yOffset += Math.min(yOffset, border);
        yOffset /= 2;
        refreshStaticFields();
    }

    private void refreshStaticFields() {
        size = cellSize;
        Point c = getLocationOnScreen();
        c.translate(getWidth()/2, getHeight()/2);
        center = getCellContains(c);
    }

    private Point last_drag_point;

    @Override
    public void mouseDragged(MouseEvent e) {
        if (last_drag_point != null) {
            move(last_drag_point, e.getPoint());
            last_drag_point = e.getPoint();
        }
    }

    @Override
    public void mouseMoved(MouseEvent e) {
    }

    @Override
    public void mouseWheelMoved(MouseWheelEvent e) {
        zoom(e.getPoint().x, e.getPoint().y, e.getWheelRotation());
    }

    @Override
    public void mouseClicked(MouseEvent e) {
    }

    @Override
    public void mousePressed(MouseEvent e) {
        if (e.getButton() == MouseEvent.BUTTON2)
            last_drag_point = e.getPoint();
    }

    @Override
    public void mouseReleased(MouseEvent e) {
        if (e.getButton() == MouseEvent.BUTTON2)
            last_drag_point = null;
    }

    @Override
    public void mouseEntered(MouseEvent e) {
    }

    @Override
    public void mouseExited(MouseEvent e) {
    }
}
