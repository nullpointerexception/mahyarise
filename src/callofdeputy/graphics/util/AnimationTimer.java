package callofdeputy.graphics.util;

import java.util.ArrayList;
import java.util.function.DoubleConsumer;

public class AnimationTimer extends Thread {
    private int stepTime, steps, initialDelay;
    private DoubleConsumer action;
    private Runnable startAction, endAction;
    private boolean terminated;
    private ArrayList<AnimationTimer> startAfter = new ArrayList<>();

    public AnimationTimer(int duration, int frequency,
                 DoubleConsumer action,
                 Runnable startAction, Runnable endAction) {
        steps = duration * frequency / 1000;
        stepTime = 1000 / frequency;
        this.startAction = startAction;
        this.action = action;
        this.endAction = endAction;
    }

    public AnimationTimer(int duration, int frequency, DoubleConsumer action) {
        this(duration, frequency, action, null, null);
    }

    @Override
    public void run() {
        try {
            sleep(initialDelay);
        } catch (InterruptedException ignored) {
        }
        if (startAction != null)
            startAction.run();
        long delay = 0, s, e;
        for (int step = 0; step < steps; step++) {
            s = System.currentTimeMillis();
            if (terminated)
                break;
            // do actions
            if (action != null)
                action.accept((double)step/steps);
            try {
                sleep(delay + stepTime);
            } catch (InterruptedException ignored) {
            } catch (Exception exc) {
                exc.printStackTrace();
            }
            // calculate delay
            e = System.currentTimeMillis();
            delay += stepTime - (e - s);
        }
        if (!terminated && action != null)
            action.accept(1.0);
        if (endAction != null)
            endAction.run();
        complete();
    }

    public synchronized void waitToComplete() {
        try {
            while (!terminated)
                wait();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    public synchronized void startAfter(AnimationTimer timer) {
        if (timer != null)
            timer.startAfter.add(this);
    }

    private synchronized void complete() {
        terminated = true;
        for (AnimationTimer timer : startAfter)
            timer.start();
        notifyAll();
    }

    public synchronized void terminate() {
        terminated = true;
        if (getState() == State.TIMED_WAITING)
            interrupt();
    }

    public void setInitialDelay(int initialDelay) {
        this.initialDelay = initialDelay;
    }
}