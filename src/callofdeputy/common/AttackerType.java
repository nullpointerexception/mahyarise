package callofdeputy.common;

import callofdeputy.client.Team;

public enum AttackerType
{
    CE_INFANTRY(400, 20, 200, 4, 10, (int)(10 * 0.8)),
    CE_TANK(1000, 100, 500, 6, 40, (int)(40 * 0.8)),
    MATH_INFANTRY(400, 20, 200, 4, 10, (int)(10 * 0.8)),
    MATH_TANK(1000, 100, 500, 6, 40, (int)(40 * 0.8));
    
    private int health;
    private int power;
    private int reloadTime;
    private int range;
    private int price;//gheimat
    public boolean AtuoHealing = false;
    public boolean hasShield = false;
    private int value;//arzesh


    public int getValue() {
        return value;
    }

    public void setValue(int value) {
        this.value = value;
    }

    public boolean hasAtuoHealing() {
        return AtuoHealing;
    }

    private AttackerType(int health, int power, int reloadTime, int range,
            int price, int value) {
        this.health = health;
        this.power = power;
        this.reloadTime = reloadTime;
        this.range = range;
        this.price = price;
        this.value = value;
    }

	public int getHealth() {
		return health;
	}

	public void setHealth(int health) {
		this.health = health;
	}

	public int getPower() {
		return power;
	}

	public void setPower(int power) {
		this.power = power;
	}

	public int getReloadTime() {
		return reloadTime;
	}

	public void setReloadTime(int reloadTime) {
		this.reloadTime = reloadTime;
	}

	public int getRange() {
		return range;
	}

	public void setRange(int range) {
		this.range = range;
	}

	public int getPrice() {
		return price;
	}

	public void setPrice(int price) {
		this.price = price;
	}


    public void powerup(Team team, PowerupType type){

        if ( type.equals(PowerupType.ATTACKER_HEALTH)){
            this.health += 5;
            this.value += this.price * (0.05);
        }

        if ( type.equals(PowerupType.ATTACKER_POWER)) {
            this.power *= (1.1);
            this.value += this.price * (0.05);
        }

        if ( type.equals(PowerupType.CE_PACE)){
            CE_TANK.reloadTime = 400;
            CE_INFANTRY.value += this.price * (0.05);
        }

        if ( type.equals(PowerupType.CE_HEALTH)){
            CE_TANK.AtuoHealing = true;
            CE_TANK.value += this.price * (0.1);
            CE_INFANTRY.AtuoHealing = true;
            CE_INFANTRY.value += this.price * (0.1);
        }

        if ( type.equals(PowerupType.CE_ARMOR)){
            CE_TANK.hasShield = true;
            CE_TANK.value += this.price * (0.05);
            CE_INFANTRY.hasShield = true;
            CE_INFANTRY.value += this.price * (0.05);
        }
    }

	@Override
	public String toString() {
		return super.toString() +
				": health=" + getHealth() +
				", power=" + getPower() +
				", reloadTime=" + getReloadTime() +
				", range=" + getRange() +
				", price=" + getPrice();
	}
}
