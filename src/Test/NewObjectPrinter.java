package Test;

import callofdeputy.common.GameObject;
import callofdeputy.judge.Judge;
import callofdeputy.common.exceptions.MahyariseExceptionBase;

import java.util.ArrayList;

/**
 * Created by Hadi on 5/25/14.
 */
public class NewObjectPrinter extends Thread {
    private Judge judge;
    ArrayList<GameObject> temp = new ArrayList<>();

    public NewObjectPrinter(Judge judge) {
        this.judge = judge;
        setDaemon(true);
    }

    @Override
    public void run() {
        GameObject[] objects = judge.map.getGameObjects();
        for (GameObject object : objects)
            temp.add(object);
        while (true) {
            objects = judge.map.getGameObjects();
            for (GameObject object : objects)
                if (!temp.contains(object))
                    add(object);
            try {
                sleep(50);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

    public void add(GameObject object) {
        temp.add(object);
        try {
            System.out.println("[created: " + object.getClass() + ", info: " + judge.getInfo(object.getID()) + "]");
        } catch (MahyariseExceptionBase mahyariseExceptionBase) {
            mahyariseExceptionBase.printStackTrace();
        }
    }
}