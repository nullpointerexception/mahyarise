package callofdeputy.common.map;

import java.util.ArrayList;
import java.util.List;

import callofdeputy.common.*;

public class Path {
    private int id;
    private Lane lane;
    private List<Cell> cells;
    private Building start;
    private Building end;
    
    Path(Building start, Building end, Lane lane, int id) {
        this.id = id;
        this.start = start;
        this.end = end;
        this.lane = lane;
        this.cells = new ArrayList<>();
	}

    public int getID() {
        return id;
    }
    
    void addCell(Cell cell) {
        cells.add(cell);
    }

    public Lane getLane() {
        return lane;
    }

    public boolean contains(Cell cell) {
        return cells.contains(cell);
    }

    public int indexOf(Cell cell) {
        for (int i = -3; i < cells.size() + 3; i++)
            if (getCell(i) == cell)
                return i;
        return -4;
    }

    public Cell getCell(int index) {
        if (index < -3)
            return getCell(-3);
        if (index > cells.size() + 2)
            return getCell(cells.size() + 2);
        if (index < 0)
            return getCell(index+1).getCellAtDir(lane.startLineDirection.rotateCCW());
        if (index >= cells.size())
            return getCell(index-1).getCellAtDir(lane.endLineDirection.rotateCCW());
        return cells.get(index);
    }

    public Cell nextCellToward(Cell cell, TeamID team) {
        if (team == start.getOwner().getID())
            return getCell(indexOf(cell) - 1);
        else
            return getCell(indexOf(cell) + 1);
    }

    public Cell nextCellBackward(Cell cell, TeamID team) {
        if (team == start.getOwner().getID())
            return nextCellToward(cell, end.getOwner().getID());
        else
            return nextCellToward(cell, start.getOwner().getID());
    }

    public Cell getFirstCellOf(TeamID team) {
        if (team == start.getOwner().getID())
            return cells.get(0);
        else
            return cells.get(cells.size()-1);
    }

    public int getLength() {
        return cells.size();
    }

    public String toString() {
        String result = "";
        result += "[Path" + id + " from " + start.getOwner().getID() + " to " + end.getOwner().getID() + " in Lane" + lane.getID();
        for (int i = -3; i < cells.size() + 3; i++)
            result += getCell(i) + ",";
        return result + "]";
    }
}
