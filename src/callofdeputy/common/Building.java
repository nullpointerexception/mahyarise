package callofdeputy.common;

import callofdeputy.client.Team;
import callofdeputy.common.map.Cell;
import callofdeputy.common.map.CellType;
import callofdeputy.common.map.Direction;

import java.util.ArrayList;
import java.util.Collections;

public class Building extends GameObject {
    private TeamID teamIDs; // TODO

    private BuildingType type;
    private ArrayList<Cell> cells = new ArrayList<>();
    private Direction orientation;

    public Building(Team owner, BuildingType type) {
        super(type.getHealth(), 0, 0, null, owner);
        this.type = type;
    }

    public BuildingType getType() {
        return type;
    }

    public void addCell(Cell cell) {
        if (!cells.contains(cell))
            cells.add(cell);
    }

    public void complete() {
        Cell[] neighbours;
        for (int i = 0; i < cells.size(); i++) {
            Cell cell = cells.get(i);
            neighbours = cell.getNeighbours();
            for (Cell neighbour : neighbours)
                if (neighbour != null
                        && neighbour.getType() == CellType.valueOf(type.toString())
                        && !cells.contains(neighbour))
                    cells.add(neighbour);
        }
        Collections.sort(cells);
    }

    public boolean contains(Cell cell) {
        return Collections.binarySearch(cells, cell) >= 0;
    }

    public Cell[] getCells() {
        return cells.toArray(new Cell[cells.size()]);
    }

    public Cell firstCell() {
        return cells.get(0);
    }

    public Cell lastCell() {
        return cells.get(cells.size() - 1);
    }

    public Cell[] getNeighbours() {
        ArrayList<Cell> neighbours = new ArrayList<Cell>();
        for (Cell cell : cells) {
            Cell[] cellNeighbours = cell.getNeighbours();
            for (Cell cellNeighbour : cellNeighbours)
                if (!cells.contains(cellNeighbour)
                        && !neighbours.contains(cellNeighbour))
                    neighbours.add(cellNeighbour);
        }
        return neighbours.toArray(new Cell[neighbours.size()]);
    }

    public void setOrientation(Direction orientation) {
        this.orientation = orientation;
    }

    public Direction getOrientation() {
        return orientation;
    }

    @Override
    public void nextStep() {
    }

    @Override
    public void finalizeStep() {
    }

    @Override
    public boolean isAlive() {
        return health > 0;
    }

    @Override
    public GameObject getTarget() {
        return null;
    }

    public String toString() {
        String result = "";
        Cell[] cells = getCells();
        result += "[Building: " + type + ", Cells: ";
        for (Cell cell : cells)
            result += cell + ", ";
        return result + "]";
    }
}