package callofdeputy.graphics.gameplay;

import callofdeputy.graphics.GameLogic;

import javax.swing.*;
import java.awt.*;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;

public class ConsoleArea extends JPanel {
    public static final Font FONT = new Font("Consolas", Font.PLAIN, 14);

    private JTextField input = new JTextField();
    private JTextArea output = new JTextArea();
    private String history[] = new String[20],
            temp[] = new String[history.length];
    private int historyIndex = -1;

    public ConsoleArea() {
        input.setFont(FONT);
        input.setBorder(null);
        input.setForeground(Color.black);
        input.setBackground(AutoHideMenu.bg2.brighter());
        output.setFont(FONT);
        output.setEditable(false);
        output.setWrapStyleWord(true);
        output.setBackground(AutoHideMenu.bg2.darker());
        output.setForeground(AutoHideMenu.bg2.brighter());
        JScrollPane scrollPane = new JScrollPane(output,
                ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS,
                ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
        scrollPane.getVerticalScrollBar().setPreferredSize(new Dimension(0, 0));
        scrollPane.setBorder(null);
        // listener
        input.addKeyListener(new KeyAdapter() {
            @Override
            public void keyPressed(KeyEvent e) {
                switch (e.getKeyCode()) {
                    case KeyEvent.VK_ENTER: readCommand(); break;
                    case KeyEvent.VK_UP: historyBack(); break;
                    case KeyEvent.VK_DOWN: historyNext(); break;
                }
            }
        });
        // add together
        setLayout(new BorderLayout());
        setOpaque(false);
        add(input, BorderLayout.NORTH);
        add(scrollPane, BorderLayout.CENTER);
        // debug tools
        callofdeputy.judge.Judge judge = new callofdeputy.judge.Judge();
        judge.map = GameLogic.getMap();
        consoleReader = new Test.ConsoleReader(judge);
    }

    private void historyBack() {
        if (historyIndex < history.length-1
                && history[historyIndex+1] != null)
            input.setText(history[++historyIndex]);
    }

    private void historyNext() {
        if (historyIndex == 0) {
            input.setText("");
            historyIndex--;
        }
        if (historyIndex > 0)
            input.setText(history[--historyIndex]);
    }

    private Test.ConsoleReader consoleReader;

    private void readCommand() {
        String command = input.getText();
        input.setText("");
        for (int i = 0; i < temp.length; i++)
            temp[i] = null;
        System.arraycopy(history, historyIndex+1, temp, 0, history.length-historyIndex-1);
        historyIndex = -1;
        history[0] = command;
        System.arraycopy(temp, 0, history, 1, history.length - 1);
        output.append(handleCommand(command) + "\n");
    }

    private String handleCommand(String command) {
        String output = "";
        if (command.equals("change team")) {
            GameLogic.setPlayerTeam(1 - GameLogic.getPlayerIndex());
            output = "current team is " + GameLogic.getPlayerTeam().getID();
        } else {
            try {
                output = consoleReader.handleCommand(command);
            } catch (Exception e) {
                output = "exception";
            }
        }
        return output;
    }
}
