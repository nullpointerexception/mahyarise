package callofdeputy.graphics.gameplay;

import callofdeputy.graphics.GameLogic;
import callofdeputy.graphics.util.ImageUtils;

import javax.swing.*;
import java.awt.*;
import java.awt.image.BufferedImage;

public class InfoPanel extends JPanel {
    private JLabel money = new JLabel(), time = new JLabel();
    private BufferedImage coin, clock;

    public InfoPanel() {
        // load images
        coin = ImageUtils.getOrLoad("coin.png");
        clock = ImageUtils.getOrLoad("clock.png");

        int gap = 10, fontSize = 30;
        setLayout(null);
        setOpaque(false);
        setSize(new Dimension(128, 256 + gap));
        // money
        money.setOpaque(false);
        money.setFont(new Font("Times New Roman", Font.PLAIN, fontSize));
        money.setForeground(new Color(0, 0, 0, 0.7f));
        money.setHorizontalAlignment(SwingConstants.CENTER);
        money.setVerticalAlignment(SwingConstants.CENTER);
        money.setBounds(0, 128+gap, 128, 128);
        add(money);
        // clock
        time.setOpaque(false);
        time.setFont(new Font("Times New Roman", Font.PLAIN, fontSize));
        time.setForeground(AutoHideMenu.bg2);
        time.setHorizontalAlignment(SwingConstants.CENTER);
        time.setVerticalAlignment(SwingConstants.CENTER);
        time.setBounds(0, 0, 128, 128);
        add(time);

        refreshTexts(0);
        GameLogic.getGame().addTimerTask(this::refreshTexts);
    }

    public void refreshTexts(double elapsedTime) {
        int t = (int) elapsedTime;
        String hour = String.valueOf(t/60);
        String min = String.valueOf(t%60);
        time.setText((hour.length() == 1 ? "0"+hour : hour) + ":"
                + (min.length() == 1 ? "0"+min : min));
        money.setText(String.valueOf(GameLogic.getPlayerTeam().getMoney()));
    }

    @Override
    protected void paintComponent(Graphics g) {
        g.drawImage(coin, money.getX(), money.getY(), null);
        g.drawImage(clock, time.getX(), time.getY(), null);
        super.paintComponent(g);
    }
}
