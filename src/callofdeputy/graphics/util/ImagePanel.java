package callofdeputy.graphics.util;

import javax.swing.*;
import java.awt.*;
import java.awt.image.BufferedImage;

public class ImagePanel extends JButton {
    private String bg;
    private boolean expand;
    private BufferedImage scaled;

    public ImagePanel(String bg, boolean expand) {
        this.bg = bg;
        this.expand = expand;
        BufferedImage image = ImageUtils.getOrLoad(bg);
        setPreferredSize(new Dimension(image.getWidth(), image.getHeight()));
    }

    public ImagePanel(String bg) {
        this(bg, true);
    }

    private void updateScaled() {
        scaled = ImageUtils.getRendered(bg, 0, getWidth(), getHeight(), expand);
    }

    @Override
    public Dimension getPreferredSize() {
        if (isPreferredSizeSet())
            return super.getPreferredSize();
        int size = Math.min(getParent().getWidth(), getParent().getHeight());
        return new Dimension(size, size);
    }

    @Override
    public void paint(Graphics g) {
        // fill with background color
        g.setColor(getBackground());
//        g.fillRect(0, 0, getWidth(), getHeight());
        // check for resize, etc.
        updateScaled();
        // background image
        Point trans = new Point(getWidth()/2, getHeight()/2);
        g.translate(trans.x, trans.y);
        ImageUtils.draw(g, scaled);
        g.translate(-trans.x, -trans.y);
    }
}
