package callofdeputy.graphics;

import callofdeputy.client.Game;
import callofdeputy.client.Team;
import callofdeputy.common.TeamID;
import callofdeputy.common.map.CellType;
import callofdeputy.common.map.Map;
import callofdeputy.judge.JudgeAbstract;

public class GameLogic {
    private static Team[] teams;
    private static Map map;
    private static Game game;
    private static int playerTeam;

    public static void init() {
        teams = new Team[] {
                new Team(TeamID.CE),
                new Team(TeamID.MATH)
        };
        teams[0].setOpponent(teams[1]);
        teams[1].setOpponent(teams[0]);
        game = new Game(teams, map);
    }

    public static Team getPlayerTeam() {
        return teams[playerTeam];
    }

    public static int getPlayerIndex() {
        return playerTeam;
    }

    public static Team getTeam(TeamID id) {
        switch (id) {
            case CE: return teams[0];
            case MATH: return teams[1];
        }
        return null;
    }

    public static Map getMap() {
        return map;
    }

    public static Game getGame() {
        return game;
    }

    public static void setPlayerTeam(int playerTeam) {
        GameLogic.playerTeam = playerTeam;
    }

    public static void setMap(CellType[][] types) {
        map = new Map(teams);
        map.setMapSize(types.length, types[0].length);
        map.load(types);
        game.setMap(map);
    }

    public static void setMap(int[][] types) {
        map = new Map(teams);
        map.setMapSize(types.length, types[0].length);
        for (int i = 0; i < types.length; i++)
            for (int j = 0; j < types[0].length; j++)
                setMapCellType(j, i, types[i][j]);
        map.refresh();
        game.setMap(map);
    }

    public static void setMapCellType(int col, int row, int type) {
        map.setCellType(col, row, cellTypeFromInt(type));
    }

    private static CellType cellTypeFromInt(int cellType) {
        CellType type = null;
        switch (cellType) {
            case JudgeAbstract.CELL_TYPE_LANE: type = CellType.LANE; break;
            case JudgeAbstract.CELL_TYPE_HQ: type = CellType.HQ; break;
            case JudgeAbstract.CELL_TYPE_MILITARY_BASE: type = CellType.MB; break;
            case JudgeAbstract.CELL_TYPE_UNUSED: type = CellType.UNUSED;
        }
        return type;
    }
}
