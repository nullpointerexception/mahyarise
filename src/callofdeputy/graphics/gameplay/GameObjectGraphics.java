package callofdeputy.graphics.gameplay;

import callofdeputy.common.*;
import callofdeputy.common.map.Cell;
import callofdeputy.graphics.util.ImageUtils;

import java.awt.*;
import java.awt.image.BufferedImage;

public class GameObjectGraphics {
    private static BufferedImage
            CE_HQ, MATH_HQ, MB_HOR, MB_VER, CE_INFA, CE_TANK, MATH_INFA, MATH_TANK,
            TOWER_BLACK, TOWER_ELEC, TOWER_GAME, TOWER_G_M, TOWER_POISON, TOWER_TANK;

    public static void renderScaled(int size) {
        double scale = size / 90.0;
        // towers
        TOWER_BLACK = ImageUtils.getRendered("tower_black.png", size, scale);
        TOWER_ELEC = ImageUtils.getRendered("tower_electricity.png", size, scale);
        TOWER_GAME = ImageUtils.getRendered("tower_game.png", size, scale);
        TOWER_G_M = ImageUtils.getRendered("tower_general_math.png", size, scale);
        TOWER_POISON = ImageUtils.getRendered("tower_poison.png", size, scale);
        TOWER_TANK = ImageUtils.getRendered("tower_tank.png", size, scale);
        // buildings
        scale = size / 45.0;
        CE_HQ = ImageUtils.getRendered("CE_HQ.png", size, scale);
        MATH_HQ = ImageUtils.getRendered("MATH_HQ.png", size, scale);
        scale = size / 58.0;
        MB_HOR = ImageUtils.getRendered("MB_HOR.png", size, scale);
        MB_VER = ImageUtils.getRendered("MB_VER.png", size, scale);
        // attackers
        scale = size / 150.0;
        CE_INFA = ImageUtils.getRendered("img_CE_INFANTRY.png", size, scale);
        CE_TANK = ImageUtils.getRendered("img_CE_TANK.png", size, scale);
        MATH_INFA = ImageUtils.getRendered("img_MATH_INFANTRY.png", size, scale);
        MATH_TANK = ImageUtils.getRendered("img_MATH_TANK.png", size, scale);
    }

    public static void paint(GameObject obj, int size, Graphics g) {
        if (obj instanceof Attacker)
            paintAttacker((Attacker)obj, size, g);
        if (obj instanceof Tower)
            paintTower((Tower)obj, size, g);
        if (obj instanceof Building)
            paintBuilding((Building)obj, size, g);
    }

    private static void paintAttacker(Attacker obj, int size, Graphics g) {
        Point translate = new Point(0, 0);
        if (obj.getMyPath() != null) {
            Cell currentCell = obj.getPosition();
            Cell nextCellToward = obj.getMyPath().nextCellToward(obj.getPosition(),
                    obj.getOwner().getOpponent().getID());

            /*
            see if the image should be moved or not
             */
            if (obj.GUIcanMove) {
                obj.passedTimer += obj.getNextTurnPeriod();
            }
            if (obj.passedTimer % obj.GUImoveTime == 0) {
                obj.GUIcanMove = obj.couldMove;
                obj.passedTimer = 0;
            }
            /*
            end of function
             */
            double t = (double) obj.passedTimer / (double) obj.GUImoveTime;

//            translate = IsometricUtils.d2_to_iso(
//                    (int) ((nextCellToward.getX() - currentCell.getX()) * size * t),
//                    (int) ((nextCellToward.getY() - currentCell.getY()) * size * t));
        }
//        g.translate(translate.x, translate.y);
        switch (obj.getType()) {
            case CE_INFANTRY: ImageUtils.draw(g, CE_INFA, 0.5, 0.85); break;
            case CE_TANK: ImageUtils.draw(g, CE_TANK, 0.5, 0.85); break;
            case MATH_INFANTRY: ImageUtils.draw(g, MATH_INFA, 0.5, 0.85); break;
            case MATH_TANK: ImageUtils.draw(g, MATH_TANK, 0.5, 0.85); break;
        }
        drawHealthBar(obj, size, g);
//        g.translate(-translate.x, -translate.y);
    }

    private static void paintTower(Tower obj, int size, Graphics g) {
        switch (obj.getType()) {
            case BLACK: ImageUtils.draw(g, TOWER_BLACK, 0.5, 0.91); break;
            case ELECTRICITY: ImageUtils.draw(g, TOWER_ELEC, 0.5, 0.91); break;
            case GAME: ImageUtils.draw(g, TOWER_GAME, 0.5, 0.91); break;
            case GENERAL_MATH: ImageUtils.draw(g, TOWER_G_M, 0.5, 0.91); break;
            case POISON: ImageUtils.draw(g, TOWER_POISON, 0.5, 0.91); break;
            case TANK: ImageUtils.draw(g, TOWER_TANK, 0.5, 0.91); break;
        }
        drawHealthBar(obj, size, g);
    }



    private static void paintBuilding(Building obj, int size, Graphics g) {
        if (!obj.isAlive())
            return;
        switch (obj.getType()) {
            case HQ:
                switch (obj.getOwner().getID()) {
                    case CE: ImageUtils.draw(g, CE_HQ, 0.5, 0.807); break;
                    case MATH: ImageUtils.draw(g, MATH_HQ, 0.5, 0.807); break;
                } break;
            case MB:
                switch (obj.getOrientation()) {
                    case RIGHT: case LEFT: ImageUtils.draw(g, MB_HOR, 0.5, 0.77); break;
                    case TOP: case BOTTOM: ImageUtils.draw(g, MB_VER, 0.5, 0.75); break;
                }
        }
        drawHealthBar(obj, size, g);
    }


    /*
       drawing the health bar
    */
    public static void drawHealthBar(GameObject obj, int size, Graphics g){
        int scale = 3;
        int healthbarX = 3 * size / scale;
        int healthbarY = 2 * size / scale / 3;
        int healthbarStartPointX = 3 * size / scale / 2;
        int healthbarStartPointY = -4 * size / scale;


        if (obj instanceof Tower){
            Tower obj1 = (Tower)obj;
            int myhealthLength = healthbarX * obj1.getHealth() / obj1.getType().getHealth();


            if (obj1.getHealth() <= (obj1.getType().getHealth() / 5)) {
                g.setColor(new Color(255, 30, 12));
                g.fillRect(healthbarStartPointX, healthbarStartPointY,myhealthLength ,healthbarY);
            }
            else if (obj1.getHealth() <= (obj1.getType().getHealth() / (2.5))){
                g.setColor(new Color(234, 255, 13));
                g.fillRect(healthbarStartPointX, healthbarStartPointY,myhealthLength ,healthbarY);
            }
            else{
                g.setColor(new Color(18, 137, 18));
                g.fillRect(healthbarStartPointX, healthbarStartPointY,myhealthLength ,healthbarY);
            }
        }


        if (obj instanceof Attacker){
            Attacker obj2 = (Attacker)obj;
            int myhealthLength = healthbarX * obj2.getHealth() / obj2.getType().getHealth();


            if (obj2.getHealth() <= (obj2.getType().getHealth() / 5)) {
                g.setColor(new Color(255, 30, 12));
                g.fillRect(healthbarStartPointX, healthbarStartPointY,myhealthLength ,healthbarY);
            }
            else if (obj2.getHealth() <= (obj2.getType().getHealth() / (2.5))){
                g.setColor(new Color(234, 255, 13));
                g.fillRect(healthbarStartPointX, healthbarStartPointY,myhealthLength ,healthbarY);
            }
            else{
                g.setColor(new Color(18, 137, 18));
                g.fillRect(healthbarStartPointX, healthbarStartPointY,myhealthLength ,healthbarY);
            }
        }

        if (obj instanceof Building){
            Building obj2 = (Building)obj;
            int myhealthLength = healthbarX * obj2.getHealth() / obj2.getType().getHealth();


            if (obj2.getHealth() <= (obj2.getType().getHealth() / 5)) {
                g.setColor(new Color(255, 30, 12));
                g.fillRect(healthbarStartPointX, healthbarStartPointY,myhealthLength ,healthbarY);
            }
            else if (obj2.getHealth() <= (obj2.getType().getHealth() / (2.5))){
                g.setColor(new Color(234, 255, 13));
                g.fillRect(healthbarStartPointX, healthbarStartPointY,myhealthLength ,healthbarY);
            }
            else{
                g.setColor(new Color(18, 137, 18));
                g.fillRect(healthbarStartPointX, healthbarStartPointY,myhealthLength ,healthbarY);
            }
        }

        g.setColor(Color.black);
        g.drawRect(healthbarStartPointX, healthbarStartPointY, healthbarX , healthbarY);
    }
}
