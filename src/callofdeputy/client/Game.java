package callofdeputy.client;

import java.io.Serializable;
import java.util.*;
import java.util.function.DoubleConsumer;

import callofdeputy.common.*;
import callofdeputy.common.map.Map;

public class Game implements Serializable {
    private static final long serialVersionUID = 1803704670257553106L;
    private List<Team> teams;
    private Map map;
    private int elapsedTime;
    private int timeStep = 50;
    public double speedRate = 1;
    private transient Timer timer;
    private transient ArrayList<DoubleConsumer> extraTasks = new ArrayList<>();
    
    public Game(Team[] teams, Map map) {
    	this.teams = Arrays.asList(teams);
    	this.map = map;
    	this.elapsedTime = 0;
	}
    
    public Team getTeam(TeamID id) {
		for (Team team : teams)
			if (team.getID() == id)
				return team;
		return null;
	}
    
    public Team[] getTeams() {
		return teams.toArray(new Team[teams.size()]);
	}
    
    public Map getMap() {
		return map;
	}

    public void setMap(Map map) {
        this.map = map;
    }
    
    public void start() {
    	// TODO
	}
    
    public void nextTurn() {
    	map.nextStep();
        elapsedTime += timeStep;
	}

    public void addTimerTask(DoubleConsumer task) {
        extraTasks.add(task);
    }

    public void startTimer() {
        if (timer != null)
            timer.cancel();
        timer = new Timer();
        TimerTask timerTask = new TimerTask() {
            @Override
            public void run() {
                nextTurn();
                for (DoubleConsumer task : extraTasks)
                    task.accept(getElapsedTime());
            }
        };
        timer.scheduleAtFixedRate(timerTask, 0, (int) (speedRate * timeStep));
	}
    
    public void pauseTimer() {
    	timer.cancel();
        timer = null;
	}
    
    public float getElapsedTime() {
		return (float) elapsedTime/1000;
	}
    
    public Attacker createAttacker(TeamID owner, AttackerType type, int path, int lane) {
        return map.createAttacker(getTeam(owner), type, path, lane);
	}
    
    public Tower createTower(TeamID owner, TowerType type, int x, int y) {
    	return map.createTower(getTeam(owner), type, x, y);
	}
    
    public void purchaseTeamPowerup(TeamID owner, PowerupType type) {
    	getTeam(owner).purchaseTeamPowerup(type);
	}
    
    public void purchaseTowerPowerup(TeamID owner, Tower tower, PowerupType type) {
    	getTeam(owner).purchaseTowerPowerup(tower, type);
	}
    
    public PowerupType[] getTeamPowerups(TeamID teamID) {
		ArrayList<PowerupType> powerups = getTeam(teamID).getTeamPowerups();
        return powerups.toArray(new PowerupType[powerups.size()]);
	}
    
    public GameObject getTarget(GameObject gameObject) {
    	return gameObject.getTarget();
	}
    
    public void setMoney(TeamID teamID, int amount) {
    	getTeam(teamID).setMoney(amount);
	}
}
