package callofdeputy.common.map;

public enum Direction {
    TOP,
    LEFT,
    BOTTOM,
    RIGHT;

    public Direction rotateCW() {
        Direction result;
        switch (this) {
            case TOP: result = RIGHT; break;
            case RIGHT: result = BOTTOM; break;
            case BOTTOM: result = LEFT; break;
            case LEFT: result = TOP; break;
            default: result = null;
        }
        return result;
    }

    public Direction rotateCCW() {
        Direction result;
        switch (this) {
            case TOP: result = LEFT; break;
            case RIGHT: result = TOP; break;
            case BOTTOM: result = RIGHT; break;
            case LEFT: result = BOTTOM; break;
            default: result = null;
        }
        return result;
    }

    public Direction inverse() {
        Direction result;
        switch (this) {
            case TOP: result = BOTTOM; break;
            case BOTTOM: result = TOP; break;
            case RIGHT: result = LEFT; break;
            case LEFT: result = RIGHT; break;
            default: result = null;
        }
        return result;
    }

    public int getX() {
        int x = 0;
        switch (this) {
            case RIGHT: x = 1; break;
            case LEFT: x = -1;
        }
        return x;
    }

    public int getY() {
        int y = 0;
        switch (this) {
            case TOP: y = -1; break;
            case BOTTOM: y = 1;
        }
        return y;
    }
}
