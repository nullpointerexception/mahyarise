package callofdeputy.graphics.util;

import javax.swing.*;
import java.awt.*;
import java.util.HashMap;

public class Screen extends JFrame {
    private HashMap<String, Page> pages = new HashMap<>();
    private GraphicsDevice device;

    public Screen(String title, GraphicsDevice device) {
        super(title, device.getDefaultConfiguration());
        this.device = device;
        setUndecorated(true);
        setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
    }

    public Screen(String title) {
        this(title, GraphicsEnvironment
                .getLocalGraphicsEnvironment().getDefaultScreenDevice());
    }

    public void appear() {
        device.setFullScreenWindow(this);
    }

    public void vanish() {
        device.setFullScreenWindow(null);
    }

    public void addPage(String name, Page page) {
        pages.put(name, page);
    }

    public Page removePage(String name) {
        return pages.remove(name);
    }

    public Page getPage(String name) {
        return pages.get(name);
    }

    public Page showPage(String name) {
        Page page = pages.get(name);
        getContentPane().removeAll();
        getContentPane().add(page);
        validate();
        page.requestFocus();
        return page;
    }

    public Page getVisiblePage() {
        try {
            return (Page)getContentPane().getComponent(0);
        } catch (ArrayIndexOutOfBoundsException e) {
            return null;
        }
    }
}