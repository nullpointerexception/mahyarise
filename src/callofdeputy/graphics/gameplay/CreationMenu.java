package callofdeputy.graphics.gameplay;

import callofdeputy.common.*;
import callofdeputy.graphics.util.ImageButton;
import callofdeputy.graphics.util.ImagePanel;

import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.function.Function;

public class CreationMenu extends ButtonMenu {
    private MapGraphics map;
    private ImageButton buttons[];
    private Function<GameObject, MouseAdapter> mlFunction =
            o -> new MouseAdapter() {
                private boolean dragging;

                @Override
                public void mousePressed(MouseEvent e) {
                    if (e.getButton() == MouseEvent.BUTTON1) {
                        dragging = true;
                        vanish();
                    } else if (dragging) {
                        e.setSource(map);
                        map.dispatchEvent(e);
                    }
                }

                @Override
                public void mouseDragged(MouseEvent e) {
                    if (dragging) {
                        map.drawSample(o, e.getLocationOnScreen());
                        e.setSource(map);
                        map.dispatchEvent(e);
                    }
                }

                @Override
                public void mouseReleased(MouseEvent e) {
                    if (e.getButton() == MouseEvent.BUTTON1 && dragging) {
                        dragging = false;
                        map.addGameObject(e.getLocationOnScreen());
                        map.deleteSample();
                    } else {
                        e.setSource(map);
                        map.dispatchEvent(e);
                    }
                }
            };

    public CreationMenu(int expandDirection) {
        super(expandDirection);
        ImagePanel addUnit = new ImagePanel("add_unit.png");
        addUnit.setBackground(bg2);
        add(addUnit);
        String files[] = new String[] {
                "c_infa",
                "c_tank",
                "c_tower_game",
                "c_tower_black",
                "c_tower_tank",
                "c_tower_general_math",
                "c_tower_electricity",
                "c_tower_poison"
        };
        buttons = new ImageButton[files.length];
        for (int i = 0; i < files.length; i++) {
            buttons[i] = new ImageButton(files[i] + ".png", files[i] + "_over.png", null);
            buttons[i].setBackground(bg2);
            add(buttons[i]);
        }
    }

    public void setActions(MapGraphics map) {
        this.map = map;
        int index = 0;
        setActionForAttacker(index++, AttackerType.CE_INFANTRY);
        setActionForAttacker(index++, AttackerType.CE_TANK);
        setActionForTower(index++, TowerType.GAME);
        setActionForTower(index++, TowerType.BLACK);
        setActionForTower(index++, TowerType.TANK);
        setActionForTower(index++, TowerType.GENERAL_MATH);
        setActionForTower(index++, TowerType.ELECTRICITY);
        setActionForTower(index++, TowerType.POISON);
    }

    private void setActionForAttacker(int index, AttackerType type) {
        MouseAdapter adapter = mlFunction.apply(
                new Attacker(type, null));
        buttons[index].addMouseListener(adapter);
        buttons[index].addMouseMotionListener(adapter);
    }

    private void setActionForTower(int index, TowerType type) {
        MouseAdapter adapter = mlFunction.apply(
                new Tower(type, null, null));
        buttons[index].addMouseListener(adapter);
        buttons[index].addMouseMotionListener(adapter);
    }
}
