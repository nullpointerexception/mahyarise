package callofdeputy.graphics.gameplay;

import callofdeputy.common.GameObject;
import callofdeputy.common.PowerupType;
import callofdeputy.common.Tower;
import callofdeputy.common.map.Cell;
import callofdeputy.graphics.GameLogic;
import callofdeputy.graphics.util.ImageButton;
import callofdeputy.graphics.util.ImagePanel;

import java.awt.*;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.function.Function;

public class PowerupMenu extends ButtonMenu {
    private MapGraphics map;
    private ImageButton buttons[];
    private Function<PowerupType, MouseAdapter> towerML =
            type -> new MouseAdapter() {
                private boolean dragging;

                @Override
                public void mousePressed(MouseEvent e) {
                    if (e.getButton() == MouseEvent.BUTTON1) {
                        dragging = true;
                        vanish();
                    } else if (dragging) {
                        e.setSource(map);
                        map.dispatchEvent(e);
                    }
                }

                @Override
                public void mouseDragged(MouseEvent e) {
                    if (dragging) {
                        Cell cell = map.getCellContains(e.getLocationOnScreen());
                        Tower dest = getDestTower(cell);
                        map.mark(cell, dest == null ? Color.red : Color.green);
                        e.setSource(map);
                        map.dispatchEvent(e);
                    }
                }

                @Override
                public void mouseReleased(MouseEvent e) {
                    if (e.getButton() == MouseEvent.BUTTON1 && dragging) {
                        map.unmark();
                        dragging = false;
                        Tower dest = getDestTower(
                                map.getCellContains(e.getLocationOnScreen()));
                        if (dest != null)
                            GameLogic.getGame().purchaseTowerPowerup(
                                    GameLogic.getPlayerTeam().getID(),
                                    dest, type);
                    } else {
                        e.setSource(map);
                        map.dispatchEvent(e);
                    }
                }

                private Tower getDestTower(Cell cell) {
                    GameObject[] objects = cell.getGameObjects();
                    for (GameObject object : objects)
                        if (object instanceof Tower)
                            if (object.getOwner() == GameLogic.getPlayerTeam())
                                return (Tower) object;
                    return null;
                }
            };
    private Function<PowerupType, MouseAdapter> teamML =
            type -> new MouseAdapter() {
                @Override
                public void mousePressed(MouseEvent e) {
                    GameLogic.getGame().purchaseTeamPowerup(
                            GameLogic.getPlayerTeam().getID(), type);
                }
            };

    public PowerupMenu(int expandDirection) {
        super(expandDirection);
        ImagePanel tower_powerups = new ImagePanel("tower_powerups.png");
        tower_powerups.setBackground(bg2);
        ImagePanel team_powerups = new ImagePanel("team_powerups.png");
        team_powerups.setBackground(bg2);
        String files[] = new String[] {
                "pu_tower_pace",
                "pu_tower_power",
                "pu_tower_range",
                "pu_tower_auto_healing",
                "pu_attacker_power",
                "pu_attacker_health",
                "pu_ce_health",
                "pu_ce_armor",
                "pu_ce_pace",
                "pu_math_economy",
//                "pu_math_profit",
//                "pu_math_dec_val"
        };
        buttons = new ImageButton[files.length];
        for (int i = 0; i < files.length; i++) {
            if (i == 0)
                add(tower_powerups);
            if (i == 4)
                add(team_powerups);
            buttons[i] = new ImageButton(files[i] + ".png",
                    files[i] + "_over.png", null);
            buttons[i].setBackground(bg2);
            add(buttons[i]);
        }
    }

    public void setActions(MapGraphics map) {
        this.map = map;
        setActionFor(buttons[0], PowerupType.TOWER_PACE);
        setActionFor(buttons[1], PowerupType.TOWER_POWER);
        setActionFor(buttons[2], PowerupType.TOWER_RANGE);
        setActionFor(buttons[3], PowerupType.TOWER_AUTO_HEALING);
        setActionFor(buttons[4], PowerupType.ATTACKER_POWER);
        setActionFor(buttons[5], PowerupType.ATTACKER_HEALTH);
        setActionFor(buttons[6], PowerupType.CE_HEALTH);
        setActionFor(buttons[7], PowerupType.CE_PACE);
        setActionFor(buttons[8], PowerupType.MATH_ECO);
//        setActionFor(buttons[9], PowerupType.MATH_PROFIT);
//        setActionFor(buttons[10], PowerupType.MATH_DEC_VAL);
    }

    private void setActionFor(ImageButton btn, PowerupType type) {
        if (type.name().contains("TOWER")) {
            MouseAdapter adapter = towerML.apply(type);
            btn.addMouseListener(adapter);
            btn.addMouseMotionListener(adapter);
        } else
            btn.addMouseListener(teamML.apply(type));
    }
}
