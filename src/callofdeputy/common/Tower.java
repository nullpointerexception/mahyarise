package callofdeputy.common;

import callofdeputy.client.Team;
import callofdeputy.common.map.Cell;
import callofdeputy.graphics.util.ClipPlayer;

import java.io.Serializable;
import java.util.*;

/**
 * this class is a subclass of GameObject
 *
 * @author senator
 * @see callofdeputy.common.GameObject
 */

public class Tower extends GameObject implements Serializable {
    private static final long serialVersionUID = -8313218187674208463L;
    private TowerType type;
    private int infaPower;
    private int tankPower;
    private int reloadTime;
    private double reflection;
    private int accuracy;
    private int range;
    private int lastAttackPassedTime = 50;
    private final int refreshTime = 50;
    private ClipPlayer clip;
    private Comparator<Attacker> targetPriority =
            Comparator.comparing((Attacker a) ->
                    a.getType().name().contains("TANK") ?
                            -(int) Math.ceil((double) a.getHealth() / type.getTankPower()) :
                            -(int) Math.ceil((double) a.getHealth() / type.getInfaPower()))
                    .thenComparing(a -> a.getValue())
                    .thenComparing(a -> Math.random());


    /**
     * returns the power of attacking infantry
     *
     * @return the power of attacking infantry
     */
    public int getInfaPower() {
        return infaPower;
    }

    /**
     * @param infaPower power for attacking infantry
     */
    public void setInfaPower(int infaPower) {
        this.infaPower = infaPower;
    }

    /**
     * return type of this tower
     *
     * @return type of this tower
     * @see callofdeputy.common.TowerType
     */
    public TowerType getType() {
        return type;
    }

    /**
     * @param type type of this tower
     */
    public void setType(TowerType type) {
        this.type = type;
    }

    /**
     * returns the power of attacking tank
     *
     * @return the power of attacking tank
     */
    public int getTankPower() {
        return tankPower;
    }

    /**
     * @param tankPower power of attacking tank
     */
    public void setTankPower(int tankPower) {
        this.tankPower = tankPower;
    }

    /**
     * returns time of reloading of this tower
     *
     * @return time of reloading of this tower
     */
    public int getReloadTime() {
        return reloadTime;
    }

    /**
     * @param reloadTime time of reloading this tower
     */
    public void setReloadTime(int reloadTime) {
        this.reloadTime = reloadTime;
    }

    /**
     * returns range of attack
     *
     * @return range of attack
     */
    public int getRange() {
        return range;
    }

    /**
     * set range of attack
     *
     * @param range of attack
     */
    public void setRange(int range) {
        this.range = range;
    }

    /**
     * returns reflect coefficient
     *
     * @return reflect coefficient
     */
    public double getReflection() {
        return reflection;
    }

    /**
     * sets reflect coefficient
     *
     * @param reflection
     */
    public void setReflection(double reflection) {
        this.reflection = reflection;
    }

    /**
     * constructor of tower
     *
     * @param type     Type of this Tower
     * @param owner    owner of this tower
     * @param position position of this tower
     */
    public Tower(TowerType type, Team owner, Cell position) {
        super(type.getHealth(), type.getPrice(), (int) (type.getPrice() * 0.8), position, owner);
        this.type = type;
        infaPower = type.getInfaPower();
        tankPower = type.getTankPower();
        reloadTime = type.getReloadTime();
        range = type.getRange();
        reflection = type.getReflection();
        if (owner != null && position != null)
            clip = new ClipPlayer("TowerAttack.wav", reloadTime, this);
    }

    /**
     * attacks the target and it's neighbours
     *
     * @param target main target of this tower
     */
    private void Attack(Attacker target) {
        int power;
        if (type.name().endsWith("INFANTRY"))
            power = infaPower;
        else
            power = tankPower;
        Attack(target.getPosition().getGameObjects(), power, 1);
        Attack(neighboursOfTarget(target), power, type.getReflection());
    }

    /**
     * reduces health of a target
     *
     * @param target the target
     * @param power  power of attack
     * @param coeff  coefficient of the power
     */
    private void Attack(GameObject target, int power, double coeff) {
        if (target instanceof Attacker && ((Attacker) target).getType().hasShield)
            target.loseHealth((int) ((power * 0.9) * coeff));
        else
            target.loseHealth((int) (power * coeff));
    }

    /**
     * reduces health of the list of targets
     *
     * @param targets list of target
     * @param power   power of attack
     * @param coeff   coefficient of the power
     */
    private void Attack(GameObject[] targets, int power, double coeff) {
        for (GameObject item : targets)
            Attack(item, power, coeff);
    }

    /**
     * returns the neighbours of a GameObject
     *
     * @param target
     * @return the neighbours of input
     */
    private GameObject[] neighboursOfTarget(GameObject target) {
        ArrayList<GameObject> neighbours = new ArrayList<>();
        for (GameObject item : target.getPosition().getObjectsInRange(1))
            if (item.owner != owner)
                neighbours.add(item);
        return neighbours.toArray(new GameObject[neighbours.size()]);
    }

    /**
     * returns the Objects that have been fallen in range
     *
     * @return objects that have been fallen in range
     */
    public GameObject[] getObjectsInRange() {
        return position.getObjectsInRange(range);
    }

    /**
     * refreshes this tower
     */
    @Override
    public void nextStep() {
        lastAttackPassedTime += refreshTime;
        if (lastAttackPassedTime > reloadTime) {
            Attacker target = getTarget();
            if (target != null) {
                Attack(target);
                lastAttackPassedTime = 0;
                clip.play();
            } else
                clip.pause();
        }
    }

    /**
     * do the last work of this step
     */
    @Override
    public void finalizeStep() {
        if (isAlive && health <= 0) {
            clip.destroy();
            isAlive = false;
            int transferredValue = value;
            if (owner.getTeamPowerups().contains(PowerupType.MATH_DEC_VAL))
                transferredValue = (int) ((double) transferredValue * 0.9);
            if (owner.getOpponent().getTeamPowerups().contains(PowerupType.MATH_PROFIT))
                transferredValue = (int) ((double) transferredValue * 1.1);
            owner.getOpponent().setMoney(this.owner.getOpponent().getMoney() + transferredValue);
            position.removeGameObject(this);
        }
    }

    /**
     * returns a object with most priority
     *
     * @return target of attacking
     */
    @Override
    public Attacker getTarget() {
        List<GameObject> inRange =
                Arrays.asList(position.getObjectsInRange(range));
        ArrayList<Attacker> inRangeAttackers = new ArrayList<>();
        for (GameObject o : inRange)
            if (o instanceof Attacker)
                inRangeAttackers.add((Attacker) o);
        if (inRangeAttackers.isEmpty())
            return null;
        Attacker max = Collections.max(inRangeAttackers, targetPriority);
        if (max.owner == owner || !max.isAlive())
            return null;
        return max;
    }
}

