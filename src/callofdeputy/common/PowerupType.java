package callofdeputy.common;

public enum PowerupType
{
    TOWER_PACE,
    TOWER_POWER,
    TOWER_RANGE,
    TOWER_AUTO_HEALING,
    ATTACKER_POWER,
    ATTACKER_HEALTH,
    CE_HEALTH,
    CE_ARMOR,
    CE_PACE,
    MATH_ECO,
    MATH_PROFIT,
    MATH_DEC_VAL
}
