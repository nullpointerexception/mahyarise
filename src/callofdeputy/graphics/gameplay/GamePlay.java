package callofdeputy.graphics.gameplay;

import callofdeputy.graphics.*;
import callofdeputy.graphics.util.AnimationTimer;
import callofdeputy.graphics.util.Page;

import javax.swing.*;
import java.awt.*;

public class GamePlay extends Page {
    JLayeredPane layeredPane = new JLayeredPane();
    MapGraphics mapGraphics = new MapGraphics();
    InfoPanel infoPanel = new InfoPanel();
    CreationMenu creationMenu = new CreationMenu(CreationMenu.UP);
    PowerupMenu powerupMenu = new PowerupMenu(PowerupMenu.RIGHT);
    ConsoleMenu consoleMenu = new ConsoleMenu(ConsoleMenu.LEFT);
    private AlphaComposite alphaComp = AlphaComposite.SrcOver.derive(1f);
    private boolean isAnimationStarted;

    public GamePlay() {
        addComponents();
    }

    @Override
    public int getRepaintTime() {
        return 50;
    }

    private void addComponents() {
        // add layered pane
        setLayout(new GridBagLayout());
        GridBagConstraints c = new GridBagConstraints();
        c.weightx = 1.0;
        c.weighty = 1.0;
        c.fill = GridBagConstraints.BOTH;
        add(layeredPane, c);

        // add map
        mapGraphics.setMap(GameLogic.getMap());
        layeredPane.add(mapGraphics);

        // add info panel
        layeredPane.add(infoPanel, (Integer) 1);

        // add menus
        creationMenu.setActions(mapGraphics);
        powerupMenu.setActions(mapGraphics);
        layeredPane.add(creationMenu);
        layeredPane.add(powerupMenu);
        layeredPane.add(consoleMenu);
        sendToTop(creationMenu);
        sendToTop(powerupMenu);
        sendToTop(consoleMenu);
    }

    private int topLayer = 2;

    public void sendToTop(AutoHideMenu menu) {
        layeredPane.setLayer(menu, topLayer++);
    }

    @Override
    public void paint(Graphics g) {
        if (!isAnimationStarted) {
            startAnimation();
            return;
        }
        super.paint(g);
        if (alphaComp.getAlpha() > 0f) {
            ((Graphics2D)g).setComposite(alphaComp);
            g.setColor(Color.black);
            g.fillRect(0, 0, getWidth(), getHeight());
        }
    }

    @Override
    public void setBounds(int x, int y, int w, int h) {
        super.setBounds(x, y, w, h);
        mapGraphics.setBounds(x, y, w, h);
        infoPanel.setLocation(x + w - infoPanel.getWidth() - 32, 32);
        creationMenu.setMaxBounds(x, y + h - 138, w, 138);
        powerupMenu.setMaxBounds(0, 0, 138, h);
        consoleMenu.setMaxBounds(x + w - 500, 0, 500, 330);
    }

    private void startAnimation() {
        isAnimationStarted = true;

        final int FPS = 1000/getRepaintTime();

        AnimationTimer fadeIn = new AnimationTimer(2000, FPS,
                t -> alphaComp = alphaComp.derive(1-(float)t));
        fadeIn.setInitialDelay(500);
        fadeIn.start();
    }
}
