package callofdeputy.graphics.gameplay;

import callofdeputy.graphics.util.ImageButton;

import javax.swing.*;
import javax.swing.Timer;
import java.awt.*;
import java.awt.event.*;

public class AutoHideMenu extends JPanel {
    // constants for direction of menu
    public static final int UP = 0, LEFT = 1, DOWN = 2, RIGHT = 3;

    // backgrounds
    public static final Color bg1 = new Color(17, 16, 4),
            bg2 = new Color(40, 37, 11).brighter().brighter();

    private int expandDir, minThickness = 10;
    private Rectangle minBound, maxBound;
    private boolean show;
    private ImageButton open, close;
    private Timer autoHide, autoShow;

    public AutoHideMenu(int expandDir) {
        setIgnoreRepaint(true);
        this.expandDir = expandDir;
        open = new ImageButton(
                "arrow" + expandDir + ".png",
                "arrow" + expandDir + "_over.png",
                null, false);
        close = new ImageButton(
                "arrow" + (expandDir + 2) % 4 + ".png",
                null, null, false);
        initComponents();
        enableEvents(Event.MOUSE_DRAG | Event.MOUSE_MOVE);
        // auto show/hide
        Toolkit.getDefaultToolkit().addAWTEventListener(
                event -> {
                    if (event instanceof MouseEvent)
                        if (SwingUtilities.isDescendingFrom(
                                (Component)event.getSource(), this)) {
                            if (event.paramString().startsWith("MOUSE_ENTERED"))
                                mouseEntered();
                        } else {
                            if (event.paramString().startsWith("MOUSE_ENTERED"))
                                mouseExited();
                        }
                },
                AWTEvent.MOUSE_EVENT_MASK);
        autoHide = new Timer(120, ae -> vanish());
        autoHide.setRepeats(false);
        autoShow = new Timer(-1, ae -> appear());
        autoShow.setRepeats(false);
    }

    private void initComponents() {
        // open close buttons
        JPanel openClose = new JPanel(new BorderLayout());
        open.setBackground(bg1);
        close.setBackground(bg1);
        close.setVisible(false);
        // for this panel
        setLayout(new BorderLayout());
        setBackground(bg1);
        // add together
        switch (expandDir) {
            case UP: openClose.add(open, BorderLayout.NORTH);
                openClose.add(close, BorderLayout.SOUTH);
                add(openClose, BorderLayout.NORTH);
                break;
            case LEFT: openClose.add(open, BorderLayout.WEST);
                openClose.add(close, BorderLayout.EAST);
                add(openClose, BorderLayout.WEST);
                break;
            case DOWN: openClose.add(open, BorderLayout.SOUTH);
                openClose.add(close, BorderLayout.NORTH);
                add(openClose, BorderLayout.SOUTH);
                break;
            case RIGHT: openClose.add(open, BorderLayout.EAST);
                openClose.add(close, BorderLayout.WEST);
                add(openClose, BorderLayout.EAST);
                break;
        }
    }

    public int getExpandDir() {
        return expandDir;
    }

    public synchronized void appear() {
        if (!show) toggle();
    }

    public synchronized void vanish() {
        if (show) toggle();
    }

    public synchronized void toggle() {
        show = !show;
        Container c = getParent().getParent();
        if (c instanceof GamePlay && show)
            ((GamePlay) c).sendToTop(this);
        refresh();
    }

    public void refresh() {
        setBounds(show ? maxBound : minBound);
        open.setVisible(!show);
        close.setVisible(show);
    }

    public void setMaxBounds(int x, int y, int w, int h) {
        maxBound = new Rectangle(x, y, w, h);
        switch (expandDir) {
            case UP: minBound = new Rectangle(x, y + h - minThickness, w, h); break;
            case LEFT: minBound = new Rectangle(x + w - minThickness, y, w, h); break;
            case DOWN: minBound = new Rectangle(x, y - h + minThickness, w, h); break;
            case RIGHT: minBound = new Rectangle(x - w + minThickness, y, w, h); break;
        }
        refreshBounds();
    }

    public void refreshBounds() {
        setBounds(show ? maxBound : minBound);
        revalidate();
    }

    public void mouseEntered() {
        autoHide.stop();
        autoShow.start();
    }

    public void mouseExited() {
        autoHide.start();
        autoShow.stop();
    }
}
