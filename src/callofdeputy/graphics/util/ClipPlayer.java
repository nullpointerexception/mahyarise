package callofdeputy.graphics.util;

import callofdeputy.common.GameObject;
import callofdeputy.common.map.Cell;
import callofdeputy.graphics.gameplay.MapGraphics;

import javax.sound.sampled.*;

import java.io.File;
import java.io.IOException;
import java.util.*;

public class ClipPlayer {
    private static ArrayList<ClipPlayer> clipsOfObjects = new ArrayList<>();
    private Clip clip;
    private boolean isPlay = false;
    private GameObject obj;

    public ClipPlayer(String clipFileName, int length) {
        System.gc();
        try {
            clip = AudioSystem.getClip();
            // Open an audio input stream.
            File file = new File("resources/sounds/" + clipFileName);
            AudioInputStream audioIn = AudioSystem.getAudioInputStream(file);
            // Get a sound clip resource.

            // Open audio clip and load samples from the audio input stream.
            clip.open(audioIn);
            if (length > 0 && length * 1000 <= clip.getMicrosecondLength())
                clip.setLoopPoints(0, (int) (length * audioIn.getFormat().getFrameRate() / 1000.0));
            audioIn.close();
        } catch (UnsupportedAudioFileException | LineUnavailableException | IOException e) {
            e.printStackTrace();
        }
        setVolume(-10f);
    }

    public ClipPlayer(String clipFileName, int length, GameObject obj) {
        this(clipFileName, length);
        obj = obj;
        clipsOfObjects.add(this);
    }


    public void playOnce() {
        clip.start();
    }

    public void play() {
        if (!isPlay) {
            clip.setFramePosition(0);
            clip.loop(Clip.LOOP_CONTINUOUSLY);
            isPlay = true;
        }

    }

    public void pause() {
        if (isPlay) {
            clip.stop();
            isPlay = false;
        }

    }

    public static void stopTimer() {
        for (ClipPlayer item : clipsOfObjects)
            item.setMute(true);
    }

    public static void startTimer() {
        for (ClipPlayer item : clipsOfObjects)
            item.setMute(false);
    }

    public void setVolume(float vol) {
        ((FloatControl) clip.getControl(FloatControl.Type.MASTER_GAIN)).setValue(vol);
    }

    public float getVolume() {
        return ((FloatControl) clip.getControl(FloatControl.Type.MASTER_GAIN)).getValue();
    }

    public void setMute(boolean mute) {
        ((BooleanControl) clip.getControl(BooleanControl.Type.MUTE)).setValue(mute);
    }

    public void fadeIn(int duration) {
        final float startVol = -80f, finalVol = getVolume();
        new AnimationTimer(duration, 10, t -> setVolume((float) (t*t*finalVol + (1-t*t)*startVol)), this::play, null).start();
    }

    public void fadeOut(int duration) {
        final float startVol = getVolume(), finalVol = -80f;
        new AnimationTimer(duration, 10, t -> setVolume((float) (t*t*finalVol + (1-t*t)*startVol)), null, this::pause).start();
    }

    public void destroy() {
        pause();
        clip.close();
        clipsOfObjects.remove(this);
    }

    public static void refreshVolume() {
        if (clipsOfObjects.isEmpty())
            return;
        Cell center = MapGraphics.center;
        int zoom = MapGraphics.size;
        int distance;
        int range = 30000 / (zoom * zoom);
        for (ClipPlayer item : clipsOfObjects) {
            distance = center.getDistance(item.obj.getPosition());
            if (distance > range) {
                item.setMute(true);
            } else {
                item.setMute(false);
                item.setVolume((float) (20* 14/ (Math.pow((double) (range * range + distance * distance), 0.5))) - 50);
            }
        }
    }
}
