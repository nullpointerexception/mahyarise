package callofdeputy.graphics.gameplay;

import java.awt.*;

public class IsometricUtils {
    public static final double cos30 = Math.cos(Math.toRadians(30));

    public static Point iso_to_2d(int x, int y) {
        return new Point((int)(x/cos30/2 - y), (int)(x/cos30/2 + y));
    }

    public static Point d3_to_iso(int x, int y, int z) {
        return new Point((int)(cos30 * (y + x)), -z + (y - x)/2);
    }

    public static Point d2_to_iso(int x, int y) {
        return d3_to_iso(x, y, 0);
    }
}