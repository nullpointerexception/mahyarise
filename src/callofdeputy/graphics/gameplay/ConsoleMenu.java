package callofdeputy.graphics.gameplay;

import javax.swing.*;
import java.awt.*;

public class ConsoleMenu extends AutoHideMenu {
    public ConsoleMenu(int expandDirection) {
        super(expandDirection);
        addComponents();
    }

    public void addComponents() {
        JPanel console = new JPanel();
        console.setLayout(new BorderLayout());
        console.setBackground(bg2);
        console.add(new ConsoleArea(), BorderLayout.CENTER);
        add(console, BorderLayout.CENTER);
    }
}
