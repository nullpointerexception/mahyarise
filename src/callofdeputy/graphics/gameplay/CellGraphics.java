package callofdeputy.graphics.gameplay;

import callofdeputy.common.GameObject;
import callofdeputy.common.map.Cell;
import callofdeputy.graphics.util.ImageUtils;

import java.awt.*;
import java.awt.image.BufferedImage;

public class CellGraphics {
    private static BufferedImage GRASS, DIRT[][] = new BufferedImage[2][2];

    public static void renderScaled(int size) {
        double scale = size / 100.0;
        GRASS = ImageUtils.getRendered("grass.png", size, scale);
        scale = size / 90.0;
        for (int i = 0; i < 2; i++)
            for (int j = 0; j < 2; j++)
                DIRT[i][j] = ImageUtils.getRendered("dirt" + i + j + ".png", size, scale);

    }

    public static void paint(Cell cell, int size, Graphics g) {
        paintCell(cell, size, g);
        paintGameObjects(cell, size, g);
    }

    public static void paintCell(Cell cell, int size, Graphics g) {
        ImageUtils.draw(g, DIRT[cell.getY()&1][cell.getX()&1], 0.5, 0.391);
        switch (cell.getType()) {
            case UNUSED: ImageUtils.draw(g, GRASS, 0.451, 0.582); break;
//            case MB: paintMark(cell, Color.red, size, g); break;
//            case HQ: paintMark(cell, Color.blue, size, g);
        }
    }

    public static void paintMark(Cell mark, Color markColor, int size, Graphics g) {
        int x = (int) (size * IsometricUtils.cos30);
        int y = size / 2;
        g.setColor(new Color(markColor.getRed(), markColor.getGreen(), markColor.getBlue(), 100));
        g.fillPolygon(new int[] {-x, 0, x, 0}, new int[] {0, y, 0, -y}, 4);
    }

    public static void paintGameObjects(Cell cell, int size, Graphics g) {
        for (GameObject object : cell.getGameObjects())
            GameObjectGraphics.paint(object, size, g);
    }
}
