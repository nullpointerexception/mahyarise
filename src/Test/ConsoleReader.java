package Test;

import callofdeputy.common.GameObjectID;
import callofdeputy.judge.Judge;
import callofdeputy.common.exceptions.MahyariseExceptionBase;

import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by Hadi on 5/25/14.
 */
public class ConsoleReader extends Thread {
    Judge judge;
    Scanner input = new Scanner(System.in);

    public ConsoleReader(Judge judge) {
        this.judge = judge;
        setDaemon(true);
    }

    @Override
    public void run() {
        while (true) {
            try {
                if (input.hasNext())
                    System.out.println(handleCommand(input.next()));
            } catch (Exception e) {
                e.printStackTrace();
            }
            try {
                sleep(50);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

    public String handleCommand(String command) throws Exception {
        if (command.equals("get"))
            return Debugger.logDel(",\n",
                    "TOWER_TYPE_GAME=" + judge.TOWER_TYPE_GAME,
                    "TOWER_TYPE_BLACK=" + judge.TOWER_TYPE_BLACK,
                    "TOWER_TYPE_TANK=" + judge.TOWER_TYPE_TANK,
                    "TOWER_TYPE_GENERAL_MATH=" + judge.TOWER_TYPE_GENERAL_MATH,
                    "TOWER_TYPE_ELECTRICITY=" + judge.TOWER_TYPE_ELECTRICITY,
                    "TOWER_TYPE_POISON=" + judge.TOWER_TYPE_POISON,
                    "ATTACKER_INFANTRY=" + judge.ATTACKER_INFANTRY,
                    "ATTACKER_TANK=" + judge.ATTACKER_TANK);
        else if (command.trim().equals("help"))
            return help();
        else if (command.matches("\\w+\\s+((create)|(c))\\s+\\d+\\s+\\d+\\s+\\d+")) {
            String[] params = command.split("\\s+");
            int teamID;
            if (params[0].equals("ce"))
                teamID = judge.TEAM_CE;
            else
                teamID = judge.TEAM_MATH;
            int type = Integer.parseInt(params[2]);
            int pathCol = Integer.parseInt(params[3]);
            int laneRow = Integer.parseInt(params[4]);
            if (type == judge.ATTACKER_INFANTRY || type == judge.ATTACKER_TANK) {
                try {
                    judge.createAttacker(teamID, type, pathCol, laneRow);
                } catch (MahyariseExceptionBase mahyariseExceptionBase) {
                    mahyariseExceptionBase.printStackTrace();
                } finally {
                    return "attacker created successfully.";
                }
            } else {
                try {
                    judge.createTower(teamID, type, pathCol, laneRow);
                } catch (MahyariseExceptionBase mahyariseExceptionBase) {
                    mahyariseExceptionBase.printStackTrace();
                } finally {
                    return "tower created successfully.";
                }
            }
        } else if (command.matches("(pause|stop) (timer|t)")) {
            judge.pauseTimer();
            return "timer paused";
        } else if (command.matches("start (timer|t)")) {
            judge.startTimer();
            return "timer started";
        } else if (command.equals("get ids"))
            return Debugger.logDel("\n", judge.idToObject.keySet().toArray());
        else if (command.matches("((getInfo)|(get info))\\s+.+")) {
            String[] parse = command.split("\\s+");
            String id = parse[parse.length-1];
            if (id.matches("\\d+"))
                id = getID(Integer.parseInt(id));
            return Debugger.log(judge.getInfo(GameObjectID.reconstruct(id)));
        } else if (command.matches("((onlineInfo)|(online info))")) {
            new OnlineInfo("All Objects", 800, 500) {
                @Override
                public String getInfo() {
                    String info = "";
                    GameObjectID[] ids = judge.idToObject.keySet().toArray(new GameObjectID[judge.idToObject.size()]);
                    try {
                        for (GameObjectID id : ids)
                            info += judge.getInfo(id).toString() + "\n";
                    } catch (MahyariseExceptionBase mahyariseExceptionBase) {
                        mahyariseExceptionBase.printStackTrace();
                    } finally {
                        return info;
                    }
                }
            }.start();
            return "online info started";
        } else if (command.matches("((onlineInfo)|(online info))\\s+.+")) {
            String[] parse = command.split("\\s+");
            String id = parse[parse.length-1];
            if (id.matches("\\d+"))
                id = getID(Integer.parseInt(id));
            GameObjectID gameObjectID = GameObjectID.reconstruct(id);
            new OnlineInfo(gameObjectID.toString()) {
                @Override
                public String getInfo() {
                    String info = "";
                    try {
                        info = judge.getInfo(gameObjectID).toString();
                    } catch (MahyariseExceptionBase mahyariseExceptionBase) {
                        mahyariseExceptionBase.printStackTrace();
                    } finally {
                        return info;
                    }
                }
            }.start();
            return "online info started";
        } else if (command.matches("((set\\s+speed)|(speed)|(ss))\\s+.*")) {
            String[] parse = command.split("\\s+");
            judge.game.speedRate = Double.parseDouble(parse[parse.length-1]);
            return "speed changed";
        } else if (command.matches("\\w+\\s+((hugeCreate)|(huge))\\s+\\d+")) {
            String[] params = command.split("\\s+");
            int teamID;
            if (params[0].equals("ce"))
                teamID = judge.TEAM_CE;
            else
                teamID = judge.TEAM_MATH;
            int lane = Integer.parseInt(params[2]);
            for (int i = 0; i < 10; i++)
                for (int path = 0; path < 5; path++)
                    try {
                        judge.createAttacker(teamID, judge.ATTACKER_TANK, path, lane);
                        judge.next50milis();
                    } catch (MahyariseExceptionBase mahyariseExceptionBase) {
                        mahyariseExceptionBase.printStackTrace();
                    }
            return "huge created";
        } else if (command.matches("(call)\\s+.*"))
            return call(command.split("\\s+")[1]);
        else if (command.toLowerCase().equals("ns")) {
            judge.game.nextTurn();
            return "next step";
        } else
            return Debugger.log("invalid command.\n\ttype help to show commands with examples.");
    }

    public String call(String function) {
        // [@id/callofdeputy.common.Building,3]
        Pattern p = Pattern.compile("(?<id>(\\[@id/(?<class>.*),\\d+\\])|(\\d+))[.](?<path>.*)");
        Matcher m = p.matcher(function);
        if (!m.find())
            return Debugger.log("invalid function call");
        String id = m.group("id");
        if (id.matches("\\d+"))
            id = getID(Integer.parseInt(id));
        String stringPath = m.group("path");
        String result = "";
        try {
            result = call(judge.idToObject.get(GameObjectID.reconstruct(id)), stringPath);
        } catch (Exception e) {
            result =  Debugger.log("Exception in function call.");
            e.printStackTrace();
        } finally {
            return result;
        }
    }

    private String call(Object obj, String path) throws Exception {
        String subPath = path.split("[.]")[0];
        Class c = obj.getClass();
        ArrayList<Field> fieldArrayList = new ArrayList<>();
        ArrayList<Method> methodArrayList = new ArrayList<>();
        while (c != Object.class) {
            fieldArrayList.addAll(Arrays.asList(c.getDeclaredFields()));
            methodArrayList.addAll(Arrays.asList(c.getDeclaredMethods()));
            c = c.getSuperclass();
        }
        Field[] fields = fieldArrayList.toArray(new Field[fieldArrayList.size()]);
        Method[] methods = methodArrayList.toArray(new Method[methodArrayList.size()]);
        for (Field field : fields)
            if (field.getName().equals(subPath)) {
                if (!field.isAccessible())
                    field.setAccessible(true);
                if (path.length() == subPath.length())
                    return Debugger.log("Field Name:", field.getName(), "Value:", field.get(obj));
                String remainPath = path.substring(subPath.length() + 1);
                return call(field.get(obj), remainPath);
            }
        Pattern functionCall = Pattern.compile("(?<name>\\w+)\\((?<arguments>.*)\\)");
        Matcher m = functionCall.matcher(subPath);
        String functionName = m.group("name");
        String arguments = m.group("arguments");
        String[] functionParams = null;
        if (!arguments.equals(""))
            functionParams = m.group("arguments").split("[,]");
        for (Method method : methods)
            if (method.getName().equals(functionName)) {
                if (!method.isAccessible())
                    method.setAccessible(true);
                if (path.length() == subPath.length())
                    return Debugger.log("Function Name:", functionName, "Return:", callMethod(method, obj, functionParams));
                String remainPath = path.substring(subPath.length() + 1);
                return call(callMethod(method, obj, functionParams), remainPath);
            }
        throw new Exception("invalid method call.");
    }

    private Object callMethod(Method method, Object obj, Object[] params) throws Exception {
        if (params == null)
            return method.invoke(obj);
        else
            return method.invoke(obj, params);
    }

    public String getID(int id) {
        return judge.idToObject.keySet().stream()
                .filter((GameObjectID gameObjectID) -> gameObjectID.getNumber() == id)
                .limit(1)
                .toArray()[0]
                .toString();
    }

    public String help() {
        String message = "=============================================== Help\nNote that each command has equivalent forms to type it easily!\n\n-help\n\tget help for commands.\n-get\n\tto get list of types. (e.g. constants that are necessary to create an object.)\n-create/c\n\tcreates an object.\n\tsyntax:\n\t\tteamName create/c type path/col lane/row\n\tExamples:\n\t\tce create 9 2 0\n\twhich is equivalent to\n\t\tce c 9 2 0\n-start timer/start t\n\tto start timer.\n\tExamples:\n\t\tstart timer\n\t\tstart t\n-pause timer/stop timer/pause t/stop t\n\tto pause timer.\n\tExamples:\n\t\tpause timer\n\t\tstop timer\n\t\tpause t\n\t\tstop t\n-get ids\n\tprints ids of all objects in the game.\n-getInfo/get info\n\tprints info of the object. note that you can use integer id instead long string id. (look examples)\n\tExamples:\n\tthese are equivalent:\n\t\tgetInfo [@id/callofdeputy.common.Building,3]\n\t\tget info [@id/callofdeputy.common.Building,3]\n\t\tgetInfo 3\n\t\tget info 3\n-set speed/speed/ss\n\tchanges speed of the game. (larger number == slower speed!)\n\tExamples:\n\t\tset speed 2\n\t\tspeed 2\n\t\tss 2\n-hugeCreate/huge\n\thuge create!!!\n\tsyntax:\n\t\tteamName hugeCreate/huge laneID\n\tExamples:\n\t\tce hugeCreate 2\n\t\tmath huge 1\n-call\n\tcalls a function and prints the returned value, or prints the value of a field.\n\tnote that you can use integer id instead of long string id. (look at examples)\n\tsyntax:\n\t\tcall [string id/integer part of the id].[Field or Function]\n\tExamples:\n\t\tcall [@id/callofdeputy.common.Building,3].getHealth()\n\t\tcall [@id/callofdeputy.common.Building,3].value\n\tabbreviation:\n\t\tcall 3.getHealth()\n\t\tcall 3.health\n\t\tcall 3.owner.money\n\t\tcall 9.getPoistion().getNeighbours()\n\t\tcall 9.myPath.getLength()\n\t\tcall 8.nextTurnPeriod\n\t\tcall 12.type.getHealth()\n\t\tcall 10.lastAttackPassedTime\n\t\tcall 13.getObjectsInRange()\n\t\tcall 14.getTarget()\n=============================================== End Of Help";
//        JFrame frame = new JFrame("Help!");
//        JTextArea textArea = new JTextArea(message, 20, 100);
//        JScrollPane scroll = new JScrollPane(textArea,
//                JScrollPane.VERTICAL_SCROLLBAR_ALWAYS, JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);
//        frame.add(scroll);
//        frame.setSize(800, 600);
//        frame.setVisible(true);
        return message;
    }
}
