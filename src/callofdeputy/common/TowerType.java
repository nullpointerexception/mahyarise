package callofdeputy.common;

public enum TowerType
{
	GAME(TeamID.CE, 50, 400, 2000, 5000, 0, 300, 7, 1),
	BLACK(TeamID.CE, 200, 40, 800, 4000, 0.30, 500, 7, 0.65),
	TANK(TeamID.CE, 100, 20, 500, 4000, 0.40, 500, 7, 0.5),
	GENERAL_MATH(TeamID.MATH, 20, 100, 500, 5000, 0, 300, 7, 0.9),
	ELECTRICITY(TeamID.MATH, 200, 50, 200, 2000, 0.10, 600, 7, 0.7),
	POISON(TeamID.MATH, 100, 50, 350, 2000, 0.70, 600, 7, 0.4);

	private TeamID owner;
	private int tankPower;
	private int infaPower;
	private int reloadTime;
	private int health;
	private double reflection;
	private int price;
	private int range;
	private double accuracy;

	private TowerType(TeamID owner, int tankPower, int infaPower, 
			int reloadTime, int health, double reflection, int price, 
			int range, double accuracy) {
		this.owner = owner;
		this.tankPower = tankPower;
		this.infaPower = infaPower;
		this.reloadTime = reloadTime;
		this.health = health;
		this.reflection = reflection;
		this.price = price;
		this.range = range;
		this.accuracy = accuracy;
	}

	public TeamID getOwner() {
		return owner;
	}

	public int getTankPower() {
		return tankPower;
	}

	public void setTankPower(int tankPower) {
		this.tankPower = tankPower;
	}

	public int getInfaPower() {
		return infaPower;
	}

	public void setInfaPower(int infaPower) {
		this.infaPower = infaPower;
	}

	public int getReloadTime() {
		return reloadTime;
	}

	public void setReloadTime(int reloadTime) {
		this.reloadTime = reloadTime;
	}

	public int getHealth() {
		return health;
	}

	public void setHealth(int health) {
		this.health = health;
	}

	public double getReflection() {
		return reflection;
	}

	public void setReflection(double reflection) {
		this.reflection = reflection;
	}

	public int getPrice() {
		return price;
	}

	public void setPrice(int price) {
		this.price = price;
	}

	public int getRange() {
		return range;
	}

	public void setRange(int range) {
		this.range = range;
	}

	public double getAccuracy() {
		return accuracy;
	}

	public void setAccuracy(double accuracy) {
		this.accuracy = accuracy;
	}

    @Override
    public String toString() {
        return super.toString() +
                ": owner=" + owner +
                ", tankPower=" + tankPower +
                ", infaPower=" + infaPower +
                ", reloadTime=" + reloadTime +
                ", health=" + health +
                ", reflection=" + reflection +
                ", price=" + price +
                ", range=" + range +
                ", accuracy=" + accuracy;
    }
}
