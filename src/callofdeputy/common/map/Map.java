package callofdeputy.common.map;

import callofdeputy.client.Team;
import callofdeputy.common.*;

import java.io.*;
import java.util.*;

public class Map implements Serializable {
    private static final long serialVersionUID = 5100939002180820695L;
    private int width;
    private int height;
    private Cell[][] cells;
    private Team[] teams = null;
    private List<GameObject> objects;
    private transient List<Path> paths;
    private transient List<Lane> lanes;
    private transient List<Building> MBs;
    private transient List<Building> HQs;

    public Map(Team[] teams) {
        this.teams = teams;
        objects = new ArrayList<>();
        lanes = new ArrayList<>();
        paths = new ArrayList<>();
        MBs = new ArrayList<>();
        HQs = new ArrayList<>();
    }

    public void setMapSize(int width, int height) {
        // This function must be called before anything else
        this.width = width;
        this.height = height;
        cells = new Cell[height][width];
        for (int i = 0; i < height; i++)
            for (int j = 0; j < width; j++)
                cells[i][j] = new Cell(this, j, i, CellType.UNUSED);
    }

    public int getWidth() {
        return width;
    }

    public int getHeight() {
        return height;
    }

    public void load(CellType[][] types) {
        // types are assigned, then refresh will be called, which loads the map
        for (int i = 0; i < height; i++)
            for (int j = 0; j < width; j++)
                cells[i][j].type = types[i][j];
        refresh();
    }

    public void loadFile(String fileName) {
        // TODO
        Scanner input = null;
        try {
            input = new Scanner(new BufferedReader(new FileReader(fileName)));
            int height = input.nextInt();
            int width = input.nextInt();
            setMapSize(width, height);
            CellType[][] types = new CellType[height][width];
            for (int i = 0; i < height; i++)
                for (int j = 0; j < width; j++)
                    types[i][j] = CellType.valueOf(input.next());
            load(types);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (input != null)
                input.close();
        }

    }

    public void setCellType(int x, int y, CellType cellType) {
        getCellAt(x, y).type = cellType;
    }

    public boolean isCellInMap(int x, int y) {
        return x >= 0 && x < width && y >= 0 && y < height;
    }

    public Cell[][] getCells() {
        return cells;
    }

    public Cell getCellAt(int x, int y) {
        if (isCellInMap(x, y))
            return cells[y][x];
        return new Cell(null, x, y, CellType.UNUSED);
    }

    public int getDistance(Cell first, Cell second) {
        // distance between two cell is max{deltaX, deltaY}
        int xDistance;
        int yDistance;
        xDistance = first.x - second.x;
        yDistance = first.y - second.y;
        if (xDistance < 0)
            xDistance *= -1;
        if (yDistance < 0)
            yDistance *= -1;
        return xDistance > yDistance ? xDistance : yDistance;
    }

    public Cell[] getCellsInRange(Cell origin, int range) {
        // returns cells at distance at most "range" that are in the same lane. "origin" is not in the returned array.
        Lane container = getLaneContains(origin);
        ArrayList<Cell> cells = new ArrayList<>();
        Collections.addAll(cells, container.getCells());
        // add cells of military bases
        Collections.addAll(cells, container.getStartBuilding().getCells());
        Collections.addAll(cells, container.getEndBuilding().getCells());
        // add cells of head quarters
        Collections.addAll(cells, HQs.get(0).getCells());
        Collections.addAll(cells, HQs.get(1).getCells());
        // distance is already defined
        cells.removeIf((cell) -> Math.max(Math.abs(cell.x - origin.x), Math.abs(cell.y - origin.y)) > range);
        cells.remove(origin);
        return cells.toArray(new Cell[cells.size()]);
    }

    public GameObject[] getObjectsInRange(Cell origin, int range) {
        ArrayList<GameObject> objects = new ArrayList<>();
        Cell[] cells = getCellsInRange(origin, range);
        // first, add objects except buildings
        for (Cell cell : cells)
            Collections.addAll(objects, cell.getGameObjects());
        // add military bases
        for (Building MB : MBs)
            for (Cell cell : cells)
                if (MB.contains(cell))
                    objects.add(MB);
        // add head quarters
        for (Building HQ : HQs)
            for (Cell cell : cells)
                if (HQ.contains(cell))
                    objects.add(HQ);
        return objects.toArray(new GameObject[objects.size()]);
    }

    public GameObject[] getObjectsAt(int x, int y) {
        return cells[y][x].getGameObjects();
    }

    public GameObject[] getGameObjects() {
        return objects.toArray(new GameObject[objects.size()]);
    }

    public GameObject[] getAliveObjects() {
        ArrayList<GameObject> aliveObjects = new ArrayList<>();
        for (GameObject object : objects)
            if (object.isAlive())
                aliveObjects.add(object);
        return aliveObjects.toArray(new GameObject[aliveObjects.size()]);
    }

    public Lane[] getLanes() {
        return lanes.toArray(new Lane[lanes.size()]);
    }

    public Lane getLane(int id) {
        for (Lane lane : lanes)
            if (lane.getID() == id)
                return lane;
        return null;
    }

    public Lane getLaneContains(Cell cell) {
        for (Lane lane : lanes)
            if (lane.contains(cell))
                return lane;
        return null;
    }

    public Path[] getPaths() {
        return paths.toArray(new Path[paths.size()]);
    }

    public Path getPathContains(Cell cell) {
        Lane lane = getLaneContains(cell);
        if (lane == null)
            return null;
        for (Path path : lane.getPaths())
            if (path.contains(cell))
                return path;
        return null;
    }

    public Building getBuildingContains(Cell cell) {
        for (Building hq : HQs)
            if (hq.contains(cell))
                return hq;
        for (Building mb : MBs)
            if (mb.contains(cell))
                return mb;
        return null;
    }

    public void refresh() {
        // this function loads map e.g. determines buildings, lanes and paths from "cells" variable
        refreshBuildings();
        refreshLanes();
    }

    private void refreshBuildings() {
        // I will find just one cell of each building and I'll create
        // new instance of that building. Then I'll add this cell to
        // the instance and call the function "complete" on the building.
        // this function completes cells of the building, e.g. identifies
        // other cells of the building.

        // determines head quarters
        Building[] HQs = new Building[] {
                new Building(teams[0], BuildingType.HQ),
                new Building(teams[1], BuildingType.HQ)
        };
        int index = 0;
        for (int j = 0; j < width; j++)
            for (int i = height-1; i >= 0; i--)
                if (cells[i][j].type == CellType.HQ) {
                    // check whether this cell is added to any building before, or not
                    boolean duplicateCell = false;
                    for (Building HQ : HQs)
                        if (HQ.contains(cells[i][j]))
                            duplicateCell = true;
                    if (duplicateCell)
                        continue;
                    HQs[index].addCell(cells[i][j]);
                    HQs[index].complete();
//                    objects.add(HQs[index]);
                    this.HQs.add(HQs[index++]);
                }

        // determines military bases. it searches cells around the head quarter of the team to find a military base.
        Building[] MBs = new Building[6];
        index = 0;
        for (Building HQ : this.HQs) {
            Cell[] neighbours = HQ.getNeighbours();
            for (Cell neighbour : neighbours) {
                boolean invalidCell = false;
                if (neighbour.type != CellType.MB)
                    invalidCell = true;
                for (int i = 0; i < index; i++)
                    if (MBs[i].contains(neighbour))
                        invalidCell = true;
                if (invalidCell)
                    continue;
                MBs[index] = new Building(HQ.getOwner(), BuildingType.MB);
                MBs[index].addCell(neighbour);
                MBs[index].complete();
//                objects.add(MBs[index]);
                this.MBs.add(MBs[index++]);
            }
        }

        // set variables of teams
        this.HQs.get(0).getOwner().setHQ(this.HQs.get(0));
        this.HQs.get(1).getOwner().setHQ(this.HQs.get(1));
        Building[] MBs0 = new Building[3];
        Building[] MBs1 = new Building[3];
        int index0 = 0, index1 = 0;
        for (Building MB : this.MBs)
            if (MB.getOwner().getID() == TeamID.CE)
                MBs0[index0++] = MB;
            else
                MBs1[index1++] = MB;
        MBs0[0].getOwner().setMBs(MBs0);
        MBs1[0].getOwner().setMBs(MBs1);
    }

    private void refreshLanes() {
        this.lanes.clear();
        this.paths.clear();

        int index = 0;
        ArrayList<Lane> lanes = new ArrayList<>();
        for (int i = 0; i < height; i++)
            for (int j = 0; j < width; j++) {
                if (cells[i][j].type != CellType.LANE)
                    continue;
                boolean duplicateCell = false;
                for (Lane lane : lanes)
                    if (lane.contains(cells[i][j]))
                        duplicateCell = true;
                if (duplicateCell)
                    continue;
                Lane newLane = new Lane(index++);
                newLane.addCell(cells[i][j]);
                newLane.complete();
                lanes.add(newLane);
            }
        this.lanes.addAll(lanes);

        for (Lane lane : lanes) {
            List<Cell> laneCells = Arrays.asList(lane.getCells());
            List<Cell> intersection = new ArrayList<>();
            for (Building MB : MBs) {
                intersection.clear();
                List<Cell> buildingBorder = Arrays.asList(MB.getNeighbours());
                for (Cell cell : buildingBorder)
                    if (laneCells.contains(cell))
                        intersection.add(cell);
                if (intersection.size() != 5)
                    continue;
                if (!lane.hasStartBuilding()) {
                    lane.setStartBuilding(MB);
                    lane.setStartLine(intersection);
                } else {
                    lane.setEndBuilding(MB);
                    lane.setEndLine(intersection);
                    lane.refreshPaths();
                }
            }
            Path[] paths = lane.getPaths();
            Collections.addAll(this.paths, paths);
        }
    }

    public synchronized Attacker createAttacker(Team owner, AttackerType type, int pathID, int laneID) {
        if (!canCreateAttacker(owner, type, pathID, laneID))
            return null;
        Attacker newAttacker = new Attacker(type, owner, getLane(laneID).getPath(pathID));
        objects.add(newAttacker);
        owner.setMoney(owner.getMoney() - type.getPrice());
        return newAttacker;
    }

    public boolean canCreateAttacker(Team owner, AttackerType type, int pathID, int laneID) {
        if (owner.getMoney() < type.getPrice())
            return false;
        Lane lane = getLane(laneID);
        if (!lane.getBuildingOf(owner).isAlive())
            return false;
        GameObject[] objects = lane.getPath(pathID).getFirstCellOf(owner.getID()).getGameObjects();
        for (GameObject o : objects)
            if (o instanceof Attacker)
                return false;
        return true;
    }

    public boolean canCreateAttackerInPath(Team owner, AttackerType type, int x, int y) {
        Lane lane = getLaneContains(getCellAt(x, y));
        Path path = getPathContains(getCellAt(x, y));
        if (path == null)
            return false;
        return canCreateAttacker(owner, type, path.getID(), lane.getID());
    }

    public synchronized Tower createTower(Team owner, TowerType type, int x, int y) {
        if (!canCreateTower(owner, type, x, y))
            return null;
        Tower newTower = new Tower(type, owner, getCellAt(x, y));
        objects.add(newTower);
        owner.setMoney(owner.getMoney() - type.getPrice());
        return newTower;
    }

    public boolean canCreateTower(Team owner, TowerType type, int x, int y) {
        if (owner.getID() != type.getOwner())
            return false;
        if (owner.getMoney() < type.getPrice())
            return false;
        if (!isCellInMap(x, y))
            return false;
        Cell position = getCellAt(x, y);
        GameObject[] objects = position.getGameObjects();
        for (GameObject o : objects)
            if (o instanceof Tower)
                return false;
        Path middle = getLaneContains(position).getMiddlePath();
        if (!middle.contains(position))
            return false;
        int index = middle.indexOf(position) - middle.indexOf(middle.getFirstCellOf(owner.getID()));
        index = Math.abs(index);
        if (2 * index > middle.getLength() + 1)
            return false;
        return true;
    }

    public synchronized void nextStep() {
        for (GameObject gameObject : objects)
            if (gameObject.isAlive())
                gameObject.nextStep();
        for (GameObject gameObject : objects)
            if (gameObject.isAlive())
                gameObject.finalizeStep();
        for (Team team : teams)
            team.nextStep();
    }

    public String toString() {
        String result = "";
        result += height + "\t" + width + "\n";
        for (int i = 0; i < height; ++i) {
            for (int j = 0; j < width; ++j)
                result += cells[i][j].type + "\t";
            result += "\n";
        }
        return result;
    }

    private void readObject(ObjectInputStream stream)
            throws IOException, ClassNotFoundException {
        width = stream.readInt();
        height = stream.readInt();
        cells = (Cell[][]) stream.readObject();
        teams = (Team[]) stream.readObject();
        lanes = new ArrayList<>();
        paths = new ArrayList<>();
        MBs = new ArrayList<>();
        HQs = new ArrayList<>();
        refresh();
        objects = (List<GameObject>) stream.readObject();
        for (GameObject object : objects)
            object.getPosition().addGameObject(object);
    }

    private void writeObject(ObjectOutputStream stream) throws IOException {
        stream.writeInt(width);
        stream.writeInt(height);
        stream.writeObject(cells);
        stream.writeObject(teams);
        ArrayList<GameObject> objects = new ArrayList<>();
        stream.writeObject(objects);
    }
}
