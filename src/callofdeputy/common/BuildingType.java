package callofdeputy.common;

public enum BuildingType {
    HQ(10000),
    MB(10000);

    private int health;

    private BuildingType(int health) {
        this.health = health;
    }

    public int getHealth() {
        return health;
    }
}
