package callofdeputy.graphics.pages;

import callofdeputy.graphics.util.ClipPlayer;
import callofdeputy.graphics.util.AnimationTimer;
import callofdeputy.graphics.util.ImageUtils;
import callofdeputy.graphics.util.Page;

import java.awt.*;
import java.awt.image.BufferedImage;

public class StartPage extends Page {
    private BufferedImage img;
    private boolean isAnimationStarted;

    public StartPage() {
        setBackground(Color.black);
        img = ImageUtils.getOrLoad("null_pointer_exception.png");
    }

    @Override
    public int getRepaintTime() {
        return 40;
    }

    @Override
    public void paint(Graphics g) {
        if (!isAnimationStarted) {
            startAnimation();
            return;
        }
        Graphics2D g2 = (Graphics2D) g;
        g2.setColor(Color.black);
        g2.fillRect(0, 0, getWidth(), getHeight());
        Point tr = new Point(getWidth()/2, getHeight()/2);
        g2.translate(tr.x, tr.y);
        g2.drawImage(img, -img.getWidth() / 2, -img.getHeight() / 2, null);
        g2.translate(-tr.x, -tr.y);
    }

    private void startAnimation() {
        isAnimationStarted = true;

        MainMenu.music = new ClipPlayer("dragon_fight.wav", -1);
        MainMenu.music.setVolume(1);

        AnimationTimer timer = new AnimationTimer(4000, 2, null,
                MainMenu.music::play,
                () -> {
                    ImageUtils.releaseMemory();
                    screen.showPage("main menu");
                });
        timer.setInitialDelay(500);
        timer.start();
    }
}
