package callofdeputy.graphics.util;

import callofdeputy.graphics.GameScreen;

import javax.swing.*;

public abstract class Page extends JPanel {
    protected GameScreen screen;

    public abstract int getRepaintTime();

    public void onShow() {}

    public void onHide() {}

    @Override
    public void addNotify() {
        super.addNotify();
        screen = (GameScreen)getRootPane().getParent();
    }
}
