package callofdeputy.common.map;

import com.sun.istack.internal.NotNull;
import callofdeputy.common.GameObject;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.Serializable;
import java.util.ArrayList;

public class Cell implements Comparable<Cell>, Serializable {
    private static final long serialVersionUID = -2883662435175642007L;
    int x, y;
    CellType type;
    Map map;
    transient ArrayList<GameObject> objects = new ArrayList<>();

    public Cell(Map map, int x, int y, CellType type) {
        this.x = x;
        this.y = y;
        this.type = type;
        this.map = map;
    }

    public Cell[] getNeighbours() {
        ArrayList<Cell> cells = new ArrayList<>();
        if (this.map.isCellInMap(x + 1, y))
            cells.add(this.map.getCellAt(x + 1, y));
        if (this.map.isCellInMap(x - 1, y))
            cells.add(this.map.getCellAt(x - 1, y));
        if (this.map.isCellInMap(x, y + 1))
            cells.add(this.map.getCellAt(x, y + 1));
        if (this.map.isCellInMap(x, y - 1))
            cells.add(this.map.getCellAt(x, y - 1));
        return cells.toArray(new Cell[cells.size()]);
    }

    public int getDistance(Cell other) {
        return this.map.getDistance(this, other);
    }

    public void setType(CellType type) {
        this.type = type;
    }

    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }

    public CellType getType() {
        return type;
    }

    public boolean hasGameObject() {
        return !objects.isEmpty();
    }

    public GameObject[] getGameObjects() {
        return objects.toArray(new GameObject[objects.size()]);
    }

    public void removeGameObject(GameObject gameObject) {
        objects.remove(gameObject);
    }

    public void addGameObject(GameObject gameObject) {
        objects.add(gameObject);
    }

    public Cell[] getCellsInRange(int range) {
        return this.map.getCellsInRange(this, range);
    }

    public Cell getCellAtDir(Direction dir) {
        if (map.isCellInMap(x + dir.getX(), y + dir.getY()))
            return map.getCellAt(x + dir.getX(), y + dir.getY());
        return null;
    }

    public GameObject[] getObjectsInRange(int range) {
        return this.map.getObjectsInRange(this, range);
    }

    public Lane getLane() {
        return this.map.getLaneContains(this);
    }

    public Path getPath() {
        return this.map.getPathContains(this);
    }

    public String toString() {
        return "(" + x + "," + y + ")";
    }

    @Override
    public int compareTo(@NotNull Cell o) {
        if (y != o.y)
            return y - o.y;
        if (x != o.x)
            return x - o.x;
        return 0;
    }

    private void readObject(ObjectInputStream stream)
            throws IOException, ClassNotFoundException {
        stream.defaultReadObject();
        objects = new ArrayList<>();
    }
}
