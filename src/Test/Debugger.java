package Test;

/**
 * Created by Hadi on 5/21/14.
 */
public class Debugger {
    public static String log(Object... os) {
        return logDel(" ", os);
    }

    public static String logDel(String delimiter, Object... os) {
//        System.out.print(os[0]);
//        for (int i = 1; i < os.length; i++)
//            System.out.print(delimiter + os[i]);
//        System.out.println();
        String result = os[0].toString();
        for (int i = 1; i < os.length; i++)
            result += delimiter + os[i];
        return result;
    }
}
