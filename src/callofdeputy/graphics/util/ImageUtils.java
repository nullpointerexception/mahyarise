package callofdeputy.graphics.util;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.geom.AffineTransform;
import java.awt.image.AffineTransformOp;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.HashMap;

public class ImageUtils {
    public static void draw(Graphics g, BufferedImage img,
                            double xPercent, double yPercent, double alpha) {
        if (img == null)
            return;
        Graphics2D g2 = (Graphics2D) g;
        Composite composite = g2.getComposite();
        g2.setComposite(AlphaComposite.SrcOver.derive((float)alpha));
        g.drawImage(img, -(int) (img.getWidth() * xPercent),
                -(int) (img.getHeight() * yPercent), null);
        g2.setComposite(composite);
    }

    public static void draw(Graphics g, BufferedImage img,
                            double xPercent, double yPercent) {
        g.drawImage(img, -(int) (img.getWidth() * xPercent),
                -(int) (img.getHeight() * yPercent), null);
    }

    public static void draw(Graphics g, BufferedImage img) {
        draw(g, img, 0.5, 0.5);
    }

    private static HashMap<String, BufferedImage>
            loadedImages = new HashMap<>(), scaledImages = new HashMap<>();

    public static void releaseMemory() {
        loadedImages.clear();
        scaledImages.clear();
    }

    public static BufferedImage getOrLoad(String file) {
        if (file == null)
            return null;
        BufferedImage img = loadedImages.get(file);
        try {
            if (img == null) {
//                loadedImages.put(file, img = ImageIO.read(new File("resources/images/" + file)));
                BufferedImage img1 = ImageIO.read(new File("resources/images/" + file));
                img = GraphicsEnvironment
                        .getLocalGraphicsEnvironment()
                        .getDefaultScreenDevice()
                        .getDefaultConfiguration()
                        .createCompatibleImage(img1.getWidth(), img1.getHeight(),
                                Transparency.TRANSLUCENT);
                Graphics g = img.createGraphics();
                g.drawImage(img1, 0, 0, null);
                g.dispose();
                loadedImages.put(file, img = img1);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return img;
    }

    public static BufferedImage getRendered(String file, int id, double scale) {
        if (file == null)
            return null;
        BufferedImage scaled = scaledImages.get(file + id);
        if (scaled != null)
            return scaled;
        BufferedImage img = getOrLoad(file);
        scaled = getScaled(img, null, scale);
        scaledImages.put(file + id, scaled);
        return scaled;
    }

    public static BufferedImage getRendered(String file, int id,
                                            int width, int height,
                                            boolean expand) {
        if (file == null)
            return null;
        BufferedImage scaled = scaledImages.get(file + id);
        if (scaled != null)
            return scaled;
        BufferedImage img = getOrLoad(file);
        double scx = (double) width / img.getWidth();
        double scy = (double) height / img.getHeight();
        double sc = expand ? Math.max(scx, scy) : Math.min(scx, scy);
        scaled = getRendered(file, id, sc);
        return scaled;
    }

    public static BufferedImage getScaled(BufferedImage img,
                                           BufferedImage scaled,
                                           double scale) {
        // check whether scaled image has correct size or not
        if (img == null || scale <= 0)
            return null;
        int scWidth = (int) (scale * img.getWidth());
        int scHeight = (int) (scale * img.getHeight());
        if (scaled != null && scWidth == scaled.getWidth()
                && scHeight == scaled.getHeight())
            return scaled;
        // if it does not have correct size
        AffineTransform trans = AffineTransform.getScaleInstance(scale, scale);
        AffineTransformOp transOp =
                new AffineTransformOp(trans, AffineTransformOp.TYPE_BICUBIC);
        return transOp.filter(img,
                new BufferedImage(scWidth, scHeight, img.getType()));
    }

    public static BufferedImage getScaled(BufferedImage img,
                                          BufferedImage scaled,
                                          int width, int height,
                                          boolean expand) {
        if (img == null)
            return null;
        double scx = (double) width / img.getWidth();
        double scy = (double) height / img.getHeight();
        double sc = expand ? Math.max(scx, scy) : Math.min(scx, scy);
        return getScaled(img, scaled, sc);
    }
}
