package callofdeputy.graphics.gameplay;

import javax.swing.*;
import java.awt.*;

public class ButtonMenu extends AutoHideMenu {
    private boolean hor;
    private JPanel grid;
    private GridBagConstraints lastConstraints;

    public ButtonMenu(int expandDir) {
        super(expandDir);
        // grid
        hor = getExpandDir() % 2 == 0;
        grid = new JPanel(new GridBagLayout());
        grid.setBackground(bg2);
        lastConstraints = new GridBagConstraints();
        lastConstraints.gridx = lastConstraints.gridy = 0;
        lastConstraints.gridwidth = lastConstraints.gridheight = 1;
        if (hor) {
            lastConstraints.weighty = 1;
            lastConstraints.fill = GridBagConstraints.VERTICAL;
        } else {
            lastConstraints.weightx = 1;
            lastConstraints.fill = GridBagConstraints.HORIZONTAL;
        }
        // scroll pane
        int horScroll = hor ? ScrollPaneConstants.HORIZONTAL_SCROLLBAR_ALWAYS :
                ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER;
        int verScroll = hor ? ScrollPaneConstants.VERTICAL_SCROLLBAR_NEVER :
                ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS;
        JScrollPane scroll = new JScrollPane(grid, verScroll, horScroll);
        adjustScrollBar(scroll.getHorizontalScrollBar());
        adjustScrollBar(scroll.getVerticalScrollBar());
        scroll.setBorder(null);
        add(scroll, BorderLayout.CENTER);
    }

    private void adjustScrollBar(JScrollBar scrollBar) {
        scrollBar.setPreferredSize(new Dimension(0, 0));
        scrollBar.setUnitIncrement(128);
    }

    @Override
    public Component add(Component comp) {
        grid.add(comp, lastConstraints);
        if (hor)
            lastConstraints.gridx++;
        else
            lastConstraints.gridy++;
        return comp;
    }
}
