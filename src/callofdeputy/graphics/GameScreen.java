package callofdeputy.graphics;

import callofdeputy.graphics.gameplay.GamePlay;
import callofdeputy.graphics.pages.MainMenu;
import callofdeputy.graphics.pages.StartPage;
import callofdeputy.graphics.util.AnimationTimer;
import callofdeputy.graphics.util.Page;
import callofdeputy.graphics.util.Screen;

import javax.swing.*;
import java.awt.*;
import java.awt.image.BufferStrategy;

public class GameScreen extends Screen {
    private Timer painter;
    private AlphaComposite blackComp = AlphaComposite.SrcOver.derive(1f);

    public GameScreen() {
        super("MahyaRise");
        setFocusable(false);
        setIgnoreRepaint(true);
        addPage("start screen", new StartPage());
        addPage("main menu", new MainMenu());
        addPage("game play", new GamePlay());
        appear();
    }

    @Override
    public BufferStrategy getBufferStrategy() {
        BufferStrategy bs = super.getBufferStrategy();
        if (bs == null)
            createBufferStrategy(3);
        return super.getBufferStrategy();
    }

    public void start() {
        showPage("main menu");
    }

    @Override
    public Page showPage(String name) {
        Page oldPage = getVisiblePage(), newPage = getPage(name);
        AnimationTimer fadeOut, fadeIn;
        fadeIn = new AnimationTimer(2000, 1000/newPage.getRepaintTime(),
                t -> blackComp = blackComp.derive(1-(float)t));
        fadeIn.setInitialDelay(500);
        if (oldPage != null) {
            fadeOut = new AnimationTimer(2000, 1000/oldPage.getRepaintTime(),
                    t -> blackComp = blackComp.derive((float) t),
                    null, () -> appearPage(name));
            fadeIn.startAfter(fadeOut);
            fadeOut.start();
        } else {
            appearPage(name);
            fadeIn.start();
        }
        return newPage;
    }

    private void appearPage(String name) {
        Page old = getVisiblePage();
        if (old != null)
            old.onHide();
        if (painter != null)
            painter.stop();
        super.showPage(name).onShow();
        BufferStrategy bs = getBufferStrategy();
        painter = new Timer(getPage(name).getRepaintTime(), e -> {
            Graphics g = bs.getDrawGraphics();
            if (!bs.contentsLost()) {
                paint(g);
                bs.show();
            }
        });
        painter.start();
    }

    @Override
    public void paint(Graphics g) {
        super.paint(g);
        if (blackComp.getAlpha() > 0f) {
            ((Graphics2D)g).setComposite(blackComp);
            g.setColor(Color.black);
            g.fillRect(0, 0, getWidth(), getHeight());
        }
    }
}
