package callofdeputy.graphics.util;

import javax.swing.*;
import java.awt.*;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.image.BufferedImage;
import java.util.HashMap;
import java.util.Map;

public class ImageButton extends JButton implements MouseListener {
    // hold loaded images (for better performance)
    private static Map<String, BufferedImage> loadedImages = new HashMap<>();

    private String images[];
    private BufferedImage scaled[];
    private boolean over, click, expand;
    private boolean drawHidden;

    public ImageButton(String bg, String over, String click, boolean expand) {
        addMouseListener(this);
        this.expand = expand;
        images = new String[] {bg, over, click};
        scaled = new BufferedImage[3];
        BufferedImage img = ImageUtils.getOrLoad(bg);
        setPreferredSize(new Dimension(img.getWidth(), img.getHeight()));
    }

    public ImageButton(String bg, String over, String click) {
        this(bg, over, click, true);
    }

    private void updateScaled() {
        for (int i = 0; i < scaled.length; i++)
            scaled[i] = ImageUtils
                    .getRendered(images[i], 0, getWidth(), getHeight(), expand);
    }

    @Override
    public void paint(Graphics g) {
        // fill with background color
        g.setColor(getBackground());
        if (isOpaque())
            g.fillRect(0, 0, getWidth(), getHeight());
        // check for resize, etc.
        updateScaled();
        // background image
        Point trans = new Point(getWidth()/2, getHeight()/2);
        g.translate(trans.x, trans.y);
        if (click && images[2] != null) {
            if (drawHidden) {
                ImageUtils.draw(g, scaled[0]);
                ImageUtils.draw(g, scaled[1]);
            }
            ImageUtils.draw(g, scaled[2]);
        } else if (over && images[1] != null) {
            if (drawHidden)
                ImageUtils.draw(g, scaled[0]);
            ImageUtils.draw(g, scaled[1]);
        } else
            ImageUtils.draw(g, scaled[0]);
        g.translate(-trans.x, -trans.y);
    }

    public void setDrawHidden(boolean drawHidden) {
        this.drawHidden = drawHidden;
    }

    public void mouseClicked(MouseEvent e) {
    }

    public void mousePressed(MouseEvent e) {
        click = true;
        repaint();
    }

    public void mouseReleased(MouseEvent e) {
        click = false;
        repaint();
    }

    public void mouseEntered(MouseEvent e) {
        over = true;
        repaint();
    }

    public void mouseExited(MouseEvent e) {
        over = false;
        repaint();
    }
}
