package callofdeputy.common;

import callofdeputy.client.Team;
import callofdeputy.common.map.*;

import java.io.Serializable;

public abstract class GameObject implements Serializable {
    private static final long serialVersionUID = -3021832110912400829L;
    protected int health;
    protected int value;
    protected int price;
    protected Cell position;
    protected int width = 1;
    protected int height = 1;
    private GameObjectID id = GameObjectID.create(getClass());
    protected Team owner;
    protected boolean isAlive = true;
    public GameObject(int health,int price,int value,Cell position,Team owner){
        this.health = health;
        this.value = value;
        this.price = price;
        this.position = position;
        this.owner = owner;
        if (this.position != null)
            this.position.addGameObject(this);
    }
    public GameObjectID getID() {
        return id;
    }

    public Team getOwner() {
        return owner;
    }

    public void setOwner(Team owner) {
        this.owner = owner;
    }
    
    public int getHealth() {
        return health;
    }

    public void setHealth(int health) {
        this.health = health;
    }

    public void loseHealth(int health) {
        if(isAlive)
            this.health -= health;
    }

    public void gainHealth(int health) {
        if(isAlive)
            this.health += health;
    }

    public boolean isAlive() {
        return isAlive;
    }
    
    public int getValue() {
        return value;
    }

    public void setValue(int value) {
        this.value = value ;
    }

    public void loseValue(int value) {
        if(isAlive)
            this.value -= value;
    }

    public void gainValue(int value){
        if(isAlive)
            this.value += value;
    }

    public int getPrice() {
        return price;
    }
    
    public void setPrice(int price) {
        this.price = price;
    }

    public void losePrice(int price) {
        if(isAlive)
            this.price -= price;
    }

    public void gainPrice(int price) {
        if(isAlive)
            this.price += price;
    }
    
    public Cell getPosition() {
        return this.position;
    }

    public void setPosition(Cell position) {
        this.position.removeGameObject(this);
        position.addGameObject(this);
        this.position = position;
    }

    public int getWidth() {
        return width;
    }
    
    public void setWidth(int width) {

        this.width = width;
    }

    public int getHeight() {
        return height;
    }
    public void setHeight(int height) {
        this.health = height;
    }
    
    public abstract void nextStep(); // to do next turn (for towers, attackers, etc.)
    public abstract void finalizeStep(); // finalize step
    public abstract GameObject getTarget();

    @Override
    public String toString() {
        return getID().toString();
    }
}
