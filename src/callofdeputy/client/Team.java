package callofdeputy.client;

import java.io.Serializable;
import java.util.ArrayList;

import callofdeputy.common.*;

public class Team implements Serializable {

    private static final long serialVersionUID = 5970960906998775202L;
    private transient Building HQ;
    private transient Building[] MBs;
    private int nextturnperiod = 50;
    //zamane gozashte shode pas az kharid powerup MATH_PROFIT
    private int MATH_PROFITsTimePassed;
    //kole zamane gozashte shode az ebtedaye bazi
    private int timePassed = 0;
    private ArrayList<PowerupType> TeamPowerups = new ArrayList<PowerupType>();
    private TeamID id;
    private int money = 5000;
    private int numberOfAttackerPowerRaising = 1;
    private int numberOfAttackerHealthRaising = 1;
    private int numberOfTowerRangeRaised = 0;

    public Team getOpponent() {
        return Opponent;
    }

    public void setOpponent(Team opponent) {
        Opponent = opponent;
    }

    private Team Opponent;

    public Team(TeamID id) {
        this.id = id;
    }

    public Building getHQ() {
        return HQ;
    }

    public void setHQ(Building HQ) {
        this.HQ = HQ;
    }

    public Building[] getMBs() {
        return MBs;
    }

    public void setMBs(Building[] MBs) {
        this.MBs = MBs;
    }

    public TeamID getID() {
        return id;
    }

    public void setMoney(int money) {
        this.money = money;
    }

    public int getMoney() {
        return money;
    }

    public ArrayList<PowerupType> getTeamPowerups() {
        return TeamPowerups;
    }

    public void purchaseTeamPowerup(PowerupType type) {

        if (type == PowerupType.ATTACKER_HEALTH)
            /*
            dar har bar call shodan in tabe be andaze 1000*n ke n
            tedad dafaate call shodan ast pool lazem darad,
            pas ba motaghayyer "numberOfAttackerHealthRaising"
            in tedad ra mohasebe mikonim, va an ra malzoom mikonim
             */
            if (getMoney() >= numberOfAttackerHealthRaising * 500) {
                this.money -= numberOfAttackerHealthRaising * 500;

                numberOfAttackerHealthRaising++;
                TeamPowerups.add(type);
                AttackerType.CE_TANK.powerup(this, type);
                AttackerType.CE_INFANTRY.powerup(this, type);
                AttackerType.MATH_TANK.powerup(this, type);
                AttackerType.MATH_INFANTRY.powerup(this, type);
            }

        if (type == PowerupType.ATTACKER_POWER)
            if (this.getMoney() >= numberOfAttackerPowerRaising * 1000) {
                this.money -= numberOfAttackerPowerRaising * 1000;
                numberOfAttackerHealthRaising++;

                TeamPowerups.add(type);
                AttackerType.CE_TANK.powerup(this, type);
                AttackerType.CE_INFANTRY.powerup(this, type);
                AttackerType.MATH_TANK.powerup(this, type);
                AttackerType.MATH_INFANTRY.powerup(this, type);
            }
        /*
        agar type marboot be powerup haye attacker va tower nabood
        pas marboot be khode team ast
         */

        /*az an jayee ke powerup haye marboot be ce va math motafavet and
        tavabee marboote joda joda call mishavand
        */

        if (this.getID().equals(TeamID.MATH))
            purchaseMATHPowerup(type);

        if (this.getID().equals(TeamID.CE))
            purchaseCEPowerup(type);
    }


    public void purchaseMATHPowerup(PowerupType type) {
        /*
        powerup haye marboot be MATH
         */
        if (type == PowerupType.MATH_DEC_VAL)
            if (!TeamPowerups.contains(type) && getMoney() >= 4000) {
                money -= 4000;
                TeamPowerups.add(type);
            }

        if (type == PowerupType.MATH_PROFIT)
            if (!TeamPowerups.contains(type) && getMoney() >= 4000) {
                this.money -= 4000;
                TeamPowerups.add(type);
            }

        if (type == PowerupType.MATH_ECO)
            if (!TeamPowerups.contains(type) && getMoney() >= 5000) {
                this.money -= 5000;
                TeamPowerups.add(type);
            }
    }

    public void purchaseCEPowerup(PowerupType type) {
        if (type == PowerupType.CE_ARMOR)
            if (!TeamPowerups.contains(type) && getMoney() >= 4000) {
                this.money -= 4000;
                TeamPowerups.add(type);
                AttackerType.CE_TANK.powerup(this, type);
                AttackerType.CE_INFANTRY.powerup(this, type);
            }

        if (type == PowerupType.CE_HEALTH)
            if (!TeamPowerups.contains(type) && this.getMoney() >= 5000) {
                this.money -= 5000;
                TeamPowerups.add(type);
                AttackerType.CE_INFANTRY.powerup(this, type);
                AttackerType.CE_TANK.powerup(this, type);
            }

        if (type == PowerupType.CE_PACE)
            if (!TeamPowerups.contains(type) && this.getMoney() >= 4000) {
                this.money -= 4000;
                TeamPowerups.add(type);
                AttackerType.CE_TANK.powerup(this, type);
            }
    }

    public void purchaseTowerPowerup(Tower tower, PowerupType type) {
        int value = tower.getValue();

        if (type == PowerupType.TOWER_AUTO_HEALING) {
            //felan chizi nagoftand!
        }

        if (type == PowerupType.TOWER_PACE)
            if (getMoney() >= (value * 0.1)) {
                money -= (value * 0.1);

                //taghieer arzesh tower
                value *= 1.1;
                tower.setValue((int) value);

                //kahesh reloadtime
                int reloadtime = tower.getReloadTime();
                reloadtime *= 0.95;
                tower.setReloadTime(reloadtime);
            }

        if (type == PowerupType.TOWER_RANGE) {
            if (numberOfTowerRangeRaised < 3)
                if (getMoney() >= (value * 0.2)) {
                    money -= (value * 0.2);

                    //afzayesh value tower
                    value *= 1.2;
                    tower.setValue(value);

                    //afzayesh range power
                    int range = tower.getRange();
                    range += 1;
                    tower.setRange(range);
                }
                //tedad dafaati ke range tower afzayesh yafte ezafe mishavad
                numberOfTowerRangeRaised++;
            }

        if (type == PowerupType.TOWER_POWER)
            if (getMoney() >= (value * 0.15)) {
                money -= (value * 0.15);

                //afzayesh value tower
                value *= 1.15;
                tower.setValue(value);

                //afzayesh ghodrat tower
                int infapower = tower.getInfaPower();
                int tankpower = tower.getTankPower();
                infapower *= 1.1;
                tankpower *= 1.1;
                tower.setInfaPower(infapower);
                tower.setTankPower(tankpower);
            }
    }

    public void nextStep() {
        timePassed += nextturnperiod;
        if (timePassed % 1000 == 0)
            money += 10;
        /*
            amaliate zir dar soorate dashtan powerup MATH_PROFIT
            adadi tasadofi bein 50 ta 1000 entekhab mikonad
            va an ra be mojoodie hesab ezafe mikonad
        */
        //agr bare avval bood nabayad time e be zamane gozashte ezaf shavad
        boolean isItTheFirstTime = true;
        if (TeamPowerups.contains(PowerupType.MATH_ECO)) {
            //ebteda check shavad ke tabe 1000ms call shavad
            if (isItTheFirstTime)
                isItTheFirstTime = false;
            else {
                MATH_PROFITsTimePassed += nextturnperiod;

                if (MATH_PROFITsTimePassed % 60000 == 0) {
                    int randomProfit = (int) Math.random();
                    randomProfit = (randomProfit % 951) + 50;
                    money += randomProfit;
                }
            }
        }
    }
}
