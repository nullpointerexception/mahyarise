package callofdeputy.common;

import callofdeputy.client.Team;
import callofdeputy.common.map.Cell;
import callofdeputy.common.map.Path;
import callofdeputy.graphics.util.ClipPlayer;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.Serializable;
import java.util.*;

public class Attacker extends GameObject implements Serializable {
    private static final long serialVersionUID = 3036517608624321067L;
    //health dar vaghe haman damage ast va yek adad manfi ast

    public boolean GUIcanMove = false;
    public boolean couldMove = false;
    public int GUImoveTime = 500;
    public int passedTimer = 0;
    public int a = 0;

    private  boolean canMove = false;
    public int getNextTurnPeriod() {
        return nextTurnPeriod;
    }
    public Path getMyPath() {
        return myPath;
    }
    private int nextTurnPeriod = 50;
    private int timePassed = 0;
    private int lastMovementTimePassed = 500;
    private int lastShotTimePassed;
    private AttackerType type;
    private transient Path myPath;
    private static int numOfCurrentClips = 0;
    public ClipPlayer shootingClip,
                        movingClip;

    public AttackerType getType() {
        return type;
    }
    //override kardan in method baraye moshakhas shodan getHealth
    public int getHealth() {
        return type.getHealth() + health;
    }

    public Attacker(AttackerType type, Team owner) {
        super(0, type.getPrice(), type.getValue(), null, owner);
        this.type = type;
    }

    public Attacker(AttackerType type, Team owner, Path path) {
        super(0, type.getPrice(), type.getValue(), path.getFirstCellOf(owner.getID()), owner);
        this.type = type;
        lastShotTimePassed = this.type.getReloadTime();
        myPath = path;
        shootingClip = new ClipPlayer(type.name().endsWith("INFANTRY") ? "InfantryShooting.wav" : "TankShooting.wav", -1, this);
        movingClip = new ClipPlayer(type.name().endsWith("INFANTRY") ? "InfantryMoving.wav" : "TankMoving.wav", -1, this);
        new ClipPlayer(type.name().endsWith("INFANTRY") ? "InfantryStart.wav" : "TankStart.wav", -1, this).playOnce();
    }

    public int calcDistance(GameObject object1, GameObject object2) {
        if (object2.position == null)
            return Integer.MAX_VALUE;
        return object1.getPosition().getDistance(object2.getPosition());
    }

    public GameObject[] inRangeEnemies() {
        ArrayList<GameObject> enemies = new ArrayList<>();
        GameObject[] inRange = position.getObjectsInRange(this.type.getRange());
        for (GameObject anInRange : inRange)
            if (anInRange.owner != this.owner)
                if (anInRange.isAlive())
                    enemies.add(anInRange);
        return enemies.toArray(new GameObject[enemies.size()]);
    }

    public int getRange() {
        return type.getRange();
    }

    public void setRange(int range) {
        type.setRange(range);
    }

    public int getPower() {
        return type.getPower();
    }

    public void setPower(int power) {
        type.setPower(power);
    }

    public int getReloadTime() {
        return type.getReloadTime();
    }

    public void setReloadTime(int reloadTime) {
        type.setReloadTime(reloadTime);
    }

    private Comparator<GameObject> targetPriority =
            Comparator.comparing(GameObject::isAlive)
                    .thenComparing(o -> o instanceof Tower)
                    .thenComparing(o -> -calcDistance(this, o))
                    .thenComparing(GameObject::getValue)
                    .thenComparing(o -> o instanceof Building &&
                            ((Building) o).getType() == BuildingType.MB)
                    .thenComparing(o -> Math.random());

    public GameObject getTarget() {
        List<GameObject> inRangeEnemies = Arrays.asList(inRangeEnemies());
        if (inRangeEnemies.isEmpty())
            return null;
        GameObject max = Collections.max(inRangeEnemies, targetPriority);
        return max.isAlive() ? max : null;
    }

    public void move(Cell destination) {
        position.removeGameObject(this);
        position = destination;
        position.addGameObject(this);
    }

    public void nextStep() {
        couldMove = false;
        GameObject target = getTarget();
        //agar powerup CE marboot be health ra dasht
        //har 60s 5/100 be janash ezafe shavad
        if (type.hasAtuoHealing()) {
            timePassed += nextTurnPeriod;//mohasebe shavad ke az vaghti ke AtuoHealing true shode, az an be bad har 60s call shavad
            if (timePassed % 1000 == 0) {
                health += type.getHealth() * 0.05;
                if (health > 0)//damage ke manfi nemitavanad bashad!!! agar manfi shod 0 mikonimash
                    health = 0;
            }
        }

        /*
        be andaze zamane tafavote nextturn ha(50 ms)
        be zamane gozashte az akharin partabe tir va akharin harekat
        ezafe mishavad.
         */
        lastMovementTimePassed += nextTurnPeriod;
        lastShotTimePassed += nextTurnPeriod;
        //agar nirooyee dar bordash nabood harekat mikonad
        if (target == null) {
            //har niroo har 500ms harekat mikonad, agar inghadr zaman az akharin harekat gozashte bood, harekat konad
            if (lastMovementTimePassed >= 500) {
                //harekat niroo
                canMove = true;
                couldMove = true;
                lastMovementTimePassed = 0;
            }
            shootingClip.pause();
        }
        //agar nirooye dar bordash bood harekat nemidonad va be an hamle mikonad
        else {
            if (lastShotTimePassed >= type.getReloadTime()) {
                lastShotTimePassed = 0;
                /****
                 pas az in ke target moshakhas shod
                 va tir be samte khane an object shelik shod
                 baiad be hame object haye ke ba an niroo
                 ham khane(cell) hastand ham in zarbe vared shavad
                 va az jane an ha kam shavad
                 */

                //halati ke target headquarter bashad ya na bayad joda barrasi shavad
                //chera ke agar khane adddi bashad momken ast chand GameObject dar yek khane bashand vali
                //dar dar halati ke building bashand momken nist
                if (target instanceof Building)
                    target.loseHealth(type.getPower());
                //dar payeen baiad havasemoon ham bashe ke hame khone haye dakhel ye cell enemy nistand
                else
                    for (GameObject object : target.position.getGameObjects()) {
                        int shotDamage;
                        if (object.owner == owner)
                            shotDamage = (int) (type.getPower() * 0.9);
                        else
                            shotDamage = type.getPower();
                        object.loseHealth(shotDamage);
                    }
                movingClip.pause();
                shootingClip.play();
            }
        }
        a++;
    }

    public void finalizeStep() {
        //moshakhas kardan in ke in niroo morde ya na
        if (type.getHealth() + health <= 0) {
            if (isAlive) {
                shootingClip.destroy();
                movingClip.destroy();
                isAlive = false;
                position.removeGameObject(this);//pas az marg an ra az map hazf konad

                int value = getValue();
                if (owner.getTeamPowerups().contains(PowerupType.MATH_DEC_VAL))
                    value *= 0.9;

                if (owner.getOpponent().getTeamPowerups().contains(PowerupType.MATH_PROFIT))
                    value *= 1.1;

                int murderBalance = owner.getOpponent().getMoney();
                owner.getOpponent().setMoney(murderBalance + value);///enteghal vajh(!)
            }
        }

        //agar tavanayee harekat dasht an ra harekat midahim
        if (isAlive && canMove) {
            movingClip.play();
            //an ra harekat midahim
            move(myPath.nextCellToward(position, owner.getOpponent().getID()));
            //sepas tavanayee harekat ra az an migirim
            canMove = false;
        }
        /*
        dar ghesmat payeen mikhohim poole hasel az koshtan in niroo ra be team marboote
        bedahim dar ebteda faghat chek mishavad ke har team aya hich yek az powerup haye
        marboot be afzayesh ya kahesh value baad az marg ra darand ya kheir
         */
    }

    private void readObject(ObjectInputStream stream)
            throws IOException, ClassNotFoundException {
        stream.defaultReadObject();
        myPath = position.getPath();
    }
}
