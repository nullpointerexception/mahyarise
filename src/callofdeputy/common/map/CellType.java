package callofdeputy.common.map;

public enum CellType {
    LANE,
    UNUSED,
    MB,
    HQ
}
